from django import template
from content.forms import QuestionForm

register = template.Library()

@register.inclusion_tag('content/questionform_tag.html')
def display_question_form():
	form = QuestionForm
	return {"form": form}