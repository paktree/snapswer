from django import template
from content.models import Category
from operator import attrgetter
from content.utils import ContentCacheManager

register = template.Library()

@register.inclusion_tag('content/category_tag.html')
def display_category():

	categories = ContentCacheManager.get_category()
	categories = sorted(categories, key=attrgetter('name'))
	return {"categories": categories}