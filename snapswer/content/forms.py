from django.forms import ModelForm
from .models import Question, Answer, Comment, Category
from easy_thumbnails.files import get_thumbnailer
from django import forms
from .utils import UrlImageManager, validate_image
import const
import os
from django.core.files import File
from sorl.thumbnail import get_thumbnail as sorl_get_thumbnail
from urlparse import urlparse

class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ["detail"]


    widgets = {
        'detail': forms.Textarea(attrs={'cols': 20, 'rows': 4})
    }

    title = forms.CharField(min_length=10, max_length=300)
    category = forms.ModelChoiceField(Category.objects.all())

    def clean_category(self):
        category_str = self.cleaned_data["category"]

        try:
            category = Category.objects.get(name=category_str)
            return category
        except Category.DoesNotExist:
            raise forms.ValidationError("Invalid category")

    def save(self, user, commit=True):
        question = super(QuestionForm, self).save(commit=False)
        
        category_str = self.cleaned_data["category"]
        category = Category.objects.get(name=category_str)

        question.category = category
        question.title = self.cleaned_data["title"]
        question.detail = self.cleaned_data["detail"]
        question.owner = user
        if commit:
            question.save()

        return question

class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ["image", "detail"]

    def save(self, user, question, commit=True):
        answer = super(AnswerForm, self).save(commit=False)
        answer.detail = self.cleaned_data["detail"]
        answer.image = self.cleaned_data["image"]
        answer.question = question
        answer.owner = user
        
        if commit:
            answer.save()
            # generate the thumbnails
            options = const.THUMBNAIL_300_0_OPTION
            #thumb_url = get_thumbnailer(answer.image).get_thumbnail(options).url
            thumb_url = answer.get_thumbnail()
        return answer

class AnswerUrlForm(forms.Form):
    image_url = forms.URLField()

    detail = forms.CharField(required=False)

    def clean_image_url(self):
        url = self.cleaned_data['image_url']

        return url

    def save(self, user, question, commit=True):
        answer = Answer()
        answer.detail = self.cleaned_data["detail"]
        answer.question = question
        answer.owner = user
        urlMgr = UrlImageManager()
        valid, imgFile, error_msg = urlMgr.save_image_to_question(question, self.clean_image_url())

        if not valid:
            raise forms.ValidationError(error_msg)

        output = urlparse(self.clean_image_url())
        answer.url = output.netloc

        answer.image.save(os.path.basename(imgFile), File(open(imgFile)))

        if commit:
            answer.save()
            # generate the thumbnails
            #options = const.THUMBNAIL_300_0_OPTION
            #thumb_url = get_thumbnailer(answer.image).get_thumbnail(options).url
            thumb_url = answer.get_thumbnail()
            
        return answer

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ["detail"]

    def save(self, user, answer, commit=True):
        comment = Comment()
        comment.answer = answer
        comment.owner = user
        comment.detail = self.cleaned_data["detail"]
        if commit:
            comment.save()

        return comment