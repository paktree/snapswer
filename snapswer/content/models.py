from django.db import models
from core.models import TimeStampedModel, VoteableModel, FollowableModel
from django.contrib.auth.models import User
from datetime import datetime
from easy_thumbnails.files import get_thumbnailer
import const
import os
import string
import random
import time
import content.utils
import core.const
from core.utils import snapswerresponse
from django.core import serializers
from django.db.models import Q
from sorl.thumbnail import get_thumbnail as sorl_get_thumbnail

def name_generator(instance, filename):
    name , ext = os.path.splitext(filename)
    new_name = str(time.time()).join(random.choice(string.ascii_uppercase + string.digits) for x in range(3))
    new_name = new_name.translate(None, ".")
    new_name = new_name + ext
    new_name = 'photos/' + new_name
    return new_name

class Category(TimeStampedModel, FollowableModel):
    name = models.CharField(max_length=100)

    @staticmethod
    def get_obj_from_name(category_name):
        try:
            category = Category.objects.get(name=category_name)
            return category
        except Category.DoesNotExist:
            return None

    def __unicode__(self):
        return self.name

class Question(TimeStampedModel, VoteableModel, FollowableModel):
    title = models.CharField(max_length='300')
    detail = models.TextField(blank=True, max_length=400)
    owner = models.ForeignKey(User)
    category = models.ForeignKey(Category)

    def __unicode__(self):
        return self.title

    @staticmethod
    def get_vote_question(user):
        query_list = []

        voted_question = user.content_question_votes.all()

        if voted_question:
            query = reduce(lambda x, y: x | y, [Q(id=question.id) for question in voted_question])
            query_list.append(query)
        
        voted_answer = user.content_answer_votes.all().select_related("question")

        if voted_answer:
            query = reduce(lambda x, y: x | y, [Q(id=answer.question.id) for answer in voted_answer])
            query_list.append(query)

        if not query_list:
            return None

        else:
            if len(query_list) == 1:
                final_q = query_list[0]
            else:
                final_q = query_list[0] 
                for query in query_list[1:]:
                    final_q = final_q | query

        result = Question.objects.select_related('answer_set').filter(final_q)
        return result

    @staticmethod
    def get_follow_questions(user):
        # get all the follow question
        # get all the following user question/answer
        # get all the category question
        following_users = user.accounts_profile_followers.all()

        user_questions = []
        user_ques_query = None
        cat_ques_query = None
        following_query = None

        query_list = []
        if following_users:
            user_ques_query = reduce(lambda x, y: x | y, [Q(owner=name) for name in following_users])
            user_questions = Question.objects.filter(user_ques_query)
            query_list.append(user_ques_query)

        categories = user.content_category_followers.all()

        cat_questions = []
        if categories:
            cat_ques_query = reduce(lambda x, y: x | y, [Q(category=cat) for cat in categories])
            cat_questions = Question.objects.filter(cat_ques_query)
            query_list.append(cat_ques_query)

        following_questions = user.content_question_followers.all()

        if following_questions:
            following_query = reduce(lambda x, y: x | y, [Q(id=question.id) for question in following_questions])
            query_list.append(following_query)

        final_q = None

        if not query_list:
            return None
        else:
            if len(query_list) == 1:
                final_q = query_list[0]
            else:
                final_q = query_list[0]
                for query in query_list[1:]:
                    final_q = final_q | query

        result_list = Question.objects.select_related('answer_set').filter(final_q)
        return result_list

    @staticmethod
    @snapswerresponse
    def to_summarize_question(question_set, user, index, more):
        summary_list = []
        data = {}
        for question in question_set:
            #answer = content.utils.ContentCacheManager.get_best_answer(question)
            answer = question.get_best_answer()

            summary = {}

            vote_by_user = False
            qs = question.votes.filter(username=user.username)
            if not qs:
                vote_by_user = False
            else:
                vote_by_user = True

            follow_by_user = False
            fs = question.followers.filter(username=user.username)
            if not fs:
                follow_by_user = False
            else:
                follow_by_user = True
        
            question = {'id': question.id, 
                        "title": question.title, 
                        "create": question.created,
                        "owner": question.owner.username,
                        "vote_count": question.get_vote_count(),
                        "follow_count": question.get_follow_count(),
                        "follow_by_user": follow_by_user,
                        "vote_by_user": vote_by_user,
                        }

            summary["question"] = question
            
            if answer:
                answer_info = answer.get_board_info()
            else:
                answer_info = Answer.get_no_answer_board_info()

            if answer_info:
                summary["answer"] = answer_info
                summary_list.append(summary)

        data["summaries"] = summary_list
        data["index"] = index
        data["hasMore"] = more
        
        return data, core.const.STATUS_CODE_SUCCESS

    @property
    def chomp_title(self):
        return self.title.rstrip('\n')

    def get_best_answer(self):
        answer_set = self.answer_set.all().order_by("created")
        max_cnt = 0
        best_ans = None
        for answer in answer_set:
            vote_cnt = answer.get_vote_count()

            if max_cnt <= vote_cnt:
                max_cnt = vote_cnt
                best_ans = answer

        if not best_ans and answer_set:
            best_ans = answer_set[0]

        return best_ans

    @staticmethod
    def delete_question(id, user):
        try:
            question = Question.objects.get(id=id)
        
            if question.owner != user:
                return False

            question.deleted = True
            question.save()
            return True

        except Question.DoesNotExist:
            return false

class Answer(TimeStampedModel, VoteableModel):
    image = models.ImageField(upload_to=name_generator, max_length=200)
    owner = models.ForeignKey(User)
    question = models.ForeignKey(Question)
    detail = models.TextField(blank=True, max_length=200)
    url = models.URLField(max_length=400)
    
    objects = models.Manager()

    def get_thumbnail(self, option=const.THUMBNAIL_300_0_OPTION):
        return sorl_get_thumbnail(self.image, '300', crop='center')

    def get_board_info(self):
        try:
            thumbnail = self.get_thumbnail(const.THUMBNAIL_300_0_OPTION);

            answer = {'id': self.id,
                      'owner': self.owner.username,
                      'thumb_url': thumbnail.url,
                      'thumb_height': thumbnail.height,
                      'thumb_width': thumbnail.width }

            return answer
        except Exception, e:
            return None

    @staticmethod
    def get_no_answer_board_info():
        # Temporary image
        answer = {'id': 0,
                  'ownder' : "",
                  'thumb_url': '/media/photos/default_snapswer.jpg',
                  'thumb_height': 300,
                  'thumb_width': 300}

        return answer

    @staticmethod
    @snapswerresponse
    def to_profile_answer(answer_set):
        answer_list = []
        for answer in answer_set:
            data = {}
            answer_info = answer.get_board_info()
            answer_list.append(answer_info)

        data = {}
        data["answers"] = answer_list

        return data, core.const.STATUS_CODE_SUCCESS

    @staticmethod
    @snapswerresponse
    def to_detail_answer_response(answer_set, index, user, more):
        data = {}
        answer_list = []

        if (answer_set.count() == 0):
            return data, core.const.STATUS_CODE_NO_SNAPSWER

        for answer in answer_set:
            try:
                thumbnail = answer.get_thumbnail(const.THUMBNAIL_300_0_OPTION)

                vote_by_user = False

                qs = answer.votes.filter(username=user.username)

                if qs:
                    vote_by_user = True

                follow_by_user = False

                fs = answer.owner.profile.followers.filter(username=user.username)

                if fs:
                    follow_by_user = True

                height = float(thumbnail.height)*(float(const.SNAPSWER_THUMBNAIL_WIDTH)/float(thumbnail.width))

                answer = {"id": answer.id,
                          "created": answer.created,
                          "owner": answer.owner.username,
                          "owner_id": answer.owner.id,
                          "owner_pic": answer.owner.profile.profile_pic.url,
                          "detail": answer.detail,
                          "image": answer.image.url,
                          "thumb": thumbnail.url,
                          "thumb_width": thumbnail.width,
                          "thumb_height": thumbnail.height,
                          "vote_count": answer.get_vote_count(),
                          "follow_count": answer.owner.profile.get_follow_count(),
                          "follow_by_user": follow_by_user,
                          "vote_by_user": vote_by_user
                }

                answer_list.append(answer)
            except Exception, e:
                print e
                pass
        data["answers"] = answer_list
        data["index"] = index
        data["hasMore"] = more

        return data, core.const.STATUS_CODE_SUCCESS

    @staticmethod
    def delete_snapswer(id, user):
        try:
            answer = Answer.objects.get(id=id)

            if answer.owner != user:
                return False

            answer.deleted = True
            answer.save()
            return True

        except Answer.DoesNotExist:
            return False;

class Comment(TimeStampedModel, VoteableModel):
    answer = models.ForeignKey(Answer)
    detail = models.TextField(max_length=200)
    owner = models.ForeignKey(User)

    @staticmethod
    @snapswerresponse
    def to_comment_response(comment_set):
        data = {}
        data["comments"] = []
        for comment in comment_set:
            comment_info = {}
            comment_info["detail"] = comment.detail
            comment_info["owner"] = comment.owner.username
            comment_info["created"] = comment.created
            data["comments"].append(comment_info)

        return data, core.const.STATUS_CODE_SUCCESS

    @staticmethod
    @snapswerresponse
    def to_add_success(answer):
        data = {}
        data["added"] = True
        data["comment_count"] = Comment.objects.filter(answer=answer).count()

        return data, core.const.STATUS_CODE_SUCCESS

    @staticmethod
    @snapswerresponse
    def to_add_fail():
        data = {}
        data["added"] = False
        return data, core.const.STATUS_CODE_SUCCESS

