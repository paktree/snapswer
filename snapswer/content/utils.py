import urllib2
import urllib
import string
import random
import os
import imghdr
from django.conf import settings
from PIL import Image
from django.core.files import File
import content.models
from django.core.cache import cache
import const
import operator
import requests

class UrlImageManager(object):

    def save_image_to_question(self, question, url):
        try:
            filename_charset = string.ascii_letters + string.digits
            filename_length = 10
            file_dir = os.path.join(settings.SANDBOX_ROOT, "photos")
            filename = ''.join(random.choice(filename_charset) for s in range(filename_length))

            filename_only = filename
            filename = os.path.join(file_dir, filename)
            opener = urllib2.build_opener()
            opener.addheaders = [('User-agent', 'Mozilla/5.0')]
            request = requests.get(url)

            with open(filename, 'wb') as f:
                f.write(request.content)
                f.close()
                
            if 'Content-Type' not in request.headers:
                return False, "", "not a valid image"

            if "image" in request.headers['Content-Type']:

                img = Image.open(filename)
                
                if not validate_image(img):
                    del img
                    return False, "", "Image size is not validate. Must be greater than 300px x 300px"

                del img
                imgType = imghdr.what(filename)

                if not imgType:
                    imgType = "jpg"

                os.rename(filename, filename+"."+imgType)
                imgFile = os.path.join(file_dir, filename_only+"."+imgType)

                return True, imgFile, ""
            else:
                return False, "", "Cannot find any image from the given URL"

        except Exception, e:
            print e
            return False, "", e

def validate_image(image):
    dimension = image.size
    if image.size[0] < 300:
        return False

    if image.size[1] < 300:
        return False

    return True

class ContentCacheManager():
    @staticmethod
    def update_cache():
        ContentCacheManager.get_most_weighted_question()
        ContentCacheManager.get_best_answer_from_question_list()
        ContentCacheManager.get_category()

    @staticmethod
    def get_most_weighted_question(ratio=(0.7,0.3)):

        result = cache.get(const.CACHE_MOST_WEIGHT_QUESTION_KEY)

        if result:
            return result
        else:
            queryset = content.models.Question.objects.all()
            question_weight_map = {}
            
            # rules: question weight 7, answer weight 3 (default), combined vote with weighted will be total count
            # if question has not answer, don't consider as candidate
            for question  in queryset:
                ans_cnt = question.answer_set.count()
                if not ans_cnt:
                    continue

                a_total_vote_cnt = 0
                q_vote_cnt = question.get_vote_count()
                answer_set = question.answer_set.all()
                
                for answer in answer_set:
                    a_vote_cnt = answer.get_vote_count()
                    a_total_vote_cnt = a_total_vote_cnt + (a_vote_cnt * ratio[1])
                
                total_weighted_vote = (q_vote_cnt * ratio[0]) + a_total_vote_cnt

                if total_weighted_vote != 0:
                    question_weight_map[question] = (q_vote_cnt * ratio[0]) + a_total_vote_cnt

            sorted_q = sorted(question_weight_map.iteritems(), key=operator.itemgetter(1), reverse=True)
            cache.set(const.CACHE_MOST_WEIGHT_QUESTION_KEY, 
                      sorted_q)
            return sorted_q[0:const.MOST_WEIGHT_QUESTION_ITEM]

    @staticmethod
    def get_best_answer_helper(question):
        answer_set = question.answer_set.all().order_by("created")
        max_cnt = 0
        best_ans = None
        for answer in answer_set:
            vote_cnt = answer.get_vote_count()
            # if vote count is the same, use the newer answer one
            if max_cnt <= vote_cnt:
                max_cnt = vote_cnt
                best_ans = answer

        # if no best_ans, set the first one as best_answer
        if not best_ans and answer_set:
            best_ans = answer_set[0]

        return best_ans

    @staticmethod
    def get_best_answer_from_question_list():

        cached_map = cache.get(const.CACHE_BEST_ANS_QUEST)

        if cached_map:
            return cached_map
            
        new_map = {}
        qs = content.models.Question.objects.all()
        
        for question in qs:
            
            best_ans = ContentCacheManager.get_best_answer_helper(question)

            if best_ans:
                new_map[question] = best_ans
        cache.set(const.CACHE_BEST_ANS_QUEST, new_map)
        return new_map

    @staticmethod
    def get_best_answer(question):
        cached_map = cache.get(const.CACHE_BEST_ANS_QUEST)
        if cached_map:
            if cached_map.has_key(question):
                return cached_map.get(question)
            else:
                # it doesn't have entry, update
                best_ans = ContentCacheManager.get_best_answer_helper(question)

                if best_ans:
                    cached_map[question] = best_ans
                    cache.set(const.CACHE_BEST_ANS_QUEST, cached_map)
                
                return best_ans
        else:
            # Update cache
            qa_map = ContentCacheManager.get_best_answer_from_question_list()
            if qa_map.has_key(question):
                return qa_map.get(question)
            else:
                return None

    @staticmethod
    def get_category():

        cache_list = cache.get(const.CACHE_CATEGORY)

        if cache_list:
            return cache_list
        else:
            categories = content.models.Category.objects.all()
            cache_list = []
            for category in categories:
                cache_list.append(category)
                cache.set(const.CACHE_CATEGORY, cache_list)

            return cache_list

