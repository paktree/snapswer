"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from .models import Question, Answer, Comment, Category
from django.contrib.auth.models import User
from StringIO import StringIO
from PIL import Image
from .forms import QuestionForm, AnswerForm, CommentForm
from django.core.files.uploadedfile import SimpleUploadedFile
import os
import re
from django.test.utils import override_settings
from .views import DisplayAPIView, LoadQuestionView, ContentSummaryView, VoteContentView, LoadSnapswerView, CommentView
from django.test.client import Client
from django.utils import simplejson
from core.utils import json_string_to_list
from datetime import datetime, date, timedelta
from django.core.cache import cache
from .utils import ContentCacheManager
import time
import const
import json
import core.const
from django.utils.timezone import now as utcnow
from core.tests import SnapswerTestCase

class QuestionTest(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

        category = Category()
        category.name = "temp2"
        category.save()

    def tearDown(self):
        cache.clear()

    def test_get_vote_question(self):
        u = User(username="tester1", password="12345")
        u.save()
        
        u2 = User(username="tester2", password="12345")
        u2.save()
        
        u3 = User(username="tester3", password="12345")
        u3.save()

        u4 = User(username="tester4", password="12345")
        u4.save()        

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u
        q.category = Category.objects.get(id=1)
        q.save()

        q.votes.add(u2)
        q.votes.add(u3)               

        q1 = Question()
        q1.title = "Who is the best actor?"
        q1.detail = "I would like to know who is the best actor"
        q1.owner = u
        q1.category = Category.objects.get(id=1)
        q1.save()

        q1.votes.add(u2)
        q1.votes.add(u3)               

        q2 = Question()
        q2.title = "Who is the best actor?"
        q2.detail = "I would like to know who is the best actor"
        q2.owner = u
        q2.category = Category.objects.get(id=1)
        q2.save()

        result = Question.get_vote_question(u2)
        self.assertEqual(result.count(), 2)

        a = Answer()
        a.owner = u3
        a.question = q2
        a.image = 'testfiles/images.jpeg'
        a.save()
        a.votes.add(u2)

        result = Question.get_vote_question(u2)
        self.assertEqual(result.count(), 3)

        result = Question.get_vote_question(u3)
        self.assertEqual(result.count(), 2)

    def test_basic_question(self):
        u = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u.save()
        
        self.assertEqual(User.objects.count(), 1)

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u
        q.category = Category.objects.get(id=1)
        q.save()

        self.assertEqual(Question.objects.count(), 1)

        client = Client()
        client.login(username="tester", password="12345")

        data = {"title" : "Who is the best of the best actor?",
                "detail": "I would like to know who is the best of the best actor",
                "category": 2}

        client.post("/content/question/update/1/", data=data, Follow=True)

        q = Question.objects.get(id=1)
        self.assertEqual(q.title, "Who is the best of the best actor?")
        self.assertEqual(q.category, Category.objects.get(id=2))
        self.assertEqual(q.detail, "I would like to know who is the best of the best actor")



    def test_most_weighted_question(self):
        for i in range(10):
            username = "tester" + str(i)
            u = User(username=username, password="12345")
            u.save()

        for i in range(1,7):
            q = Question()
            q.title = str(i)
            q.owner = User.objects.get(id=10)
            q.category = Category.objects.get(id=1)
            q.save()

            for j in range(1,4):
                a = Answer()
                a.owner = User.objects.get(id=9)
                a.question = q
                a.image = 'testfiles/images.jpeg'
                a.save()
                a.votes.add(User.objects.get(id=j))

        q1 = Question.objects.get(id=1)

        q1.votes.add(User.objects.get(id=1))

        q2 = Question.objects.get(id=2)
        for i in range(1,3):
            q2.votes.add(User.objects.get(id=i))

        q3 = Question.objects.get(id=3)
        for i in range(1,4):
            q3.votes.add(User.objects.get(id=i))

        q4 = Question.objects.get(id=4)
        for i in range(1,5):
            q4.votes.add(User.objects.get(id=i))

        q5 = Question.objects.get(id=5)
        for i in range(1,6):
            q5.votes.add(User.objects.get(id=i))

        q6 = Question.objects.get(id=6)
        for i in range(1,7):
            q6.votes.add(User.objects.get(id=i))

        weighted_map = ContentCacheManager.get_most_weighted_question()

        self.assertEqual(len(weighted_map), 6)
        self.assertEqual(weighted_map[0][0].title, "6")
        self.assertEqual(weighted_map[0][1], 5.1)

        self.assertEqual(weighted_map[1][0].title, "5")
        self.assertEqual(weighted_map[1][1], 4.4)

        self.assertEqual(weighted_map[2][0].title, "4")
        self.assertEqual(weighted_map[2][1], 3.6999999999999997)

        self.assertEqual(weighted_map[3][0].title, "3")
        self.assertEqual(weighted_map[3][1], 2.9999999999999996)

        self.assertEqual(weighted_map[4][0].title, "2")
        self.assertEqual(weighted_map[4][1], 2.3)

        self.assertEqual(weighted_map[5][0].title, "1")
        self.assertEqual(weighted_map[5][1], 1.5999999999999999)

        weighted_map = ContentCacheManager.get_most_weighted_question()
        self.assertEqual(weighted_map[0][0].title, "6")
        self.assertEqual(weighted_map[0][1], 5.1)

        self.assertEqual(weighted_map[1][0].title, "5")
        self.assertEqual(weighted_map[1][1], 4.4)

        self.assertEqual(weighted_map[2][0].title, "4")
        self.assertEqual(weighted_map[2][1], 3.6999999999999997)


    def test_most_weighted_question_2(self):
        for i in range(10):
            username = "tester" + str(i)
            u = User(username=username, password="12345")
            u.save()

        for i in range(1,7):
            q = Question()
            q.title = str(i)
            q.owner = User.objects.get(id=10)
            q.category = Category.objects.get(id=1)
            q.save()

            for j in range(1,4):
                a = Answer()
                a.owner = User.objects.get(id=9)
                a.question = q
                a.image = 'testfiles/images.jpeg'
                a.save()

        weighted_map = ContentCacheManager.get_most_weighted_question()

        self.assertEqual(len(weighted_map), 0)

    def test_get_question_by_seq_index(self):
        user = User(username="Test1", password="123456")
        user.save()

        for i in range(10):
            data = {"title": "What is the     " + str(i), "detail": "hi", "category": "1"}
            form = QuestionForm(data=data)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

            question = form.save(user)   

        # sleep for 1 seconds
        time.sleep(1)

        timestamp = time.mktime(datetime.now().timetuple())

        # add 5 more
        for i in range(5):
            data = {"title": "what is this    best str(i)" + "new", 
                "detail": "Can anyone tell me?", "category": "1"}

            form = QuestionForm(data=data)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

            question = form.save(user)

        qs, more = Question.get_by_seq_index(Question.objects.all(), 0, 5, timestamp)

        self.assertEqual(qs.count(), 5)
        self.assertEqual(more, True)

        i = 9
        for question in qs:
            self.assertEqual(question.title, "What is the     " + str(i))
            i = i - 1

        qs, more = Question.get_by_seq_index(Question.objects.all(), 5, 10, timestamp)
        self.assertEqual(qs.count(), 5)
        self.assertEqual(more, False)

        i = 4
        for question in qs:
            self.assertEqual(question.title, "What is the     " + str(i))
            i = i - 1

        qs, more = Question.get_by_seq_index(Question.objects.all(), 0, 1, timestamp)
        self.assertEqual(qs.count(), 1)
        self.assertEqual(more, True)

        self.assertEqual(qs[0].title, 'What is the     9')

        # sleep for 1 seconds
        time.sleep(1)

        timestamp = time.mktime(datetime.now().timetuple())

        # add 5 more
        for i in range(5):
            data = {"title": "what is this newest " + str(i), 
                "detail": "Can anyone tell me?", "category": "1"}

            form = QuestionForm(data=data)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

        qs , more = Question.get_by_seq_index(Question.objects.all(), 0, 15, timestamp)
        
        self.assertEqual(qs.count(), 15)
        self.assertEqual(more, False)

        self.assertEqual(qs[14].title, 'What is the     0')

        qs , more = Question.get_by_seq_index(Question.objects.all(), 0, const.ITEM_PER_LOAD, timestamp)
        
        self.assertEqual(qs.count(), 15)
        self.assertEqual(more, False)

        self.assertEqual(qs[14].title, 'What is the     0')

    def test_to_summary_json(self):
        user = User(username="Test1", password="123456")
        user.save()

        user2 = User(username="Test2", password="123456")
        user2.save()

        question = Question()
        question.title = "This is question"
        question.owner = user   
        question.category = Category.objects.get(id=1)
        question.save()     

        data = {"detail" : "It is chicken"}

        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "testfiles/images.jpeg")
        
        upload_file = open(path, 'rb')
        file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
        
        form = AnswerForm(data, file_dic)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        a = form.save(user=user, question=question)

        question_set = Question.objects.all()
        self.assertEqual(len(question_set), 1)

        data = Question.to_summarize_question(question_set, user, 0, True)
        decoded_ds = json.loads(data)

        self.assertEqual(decoded_ds["status_code"], core.const.STATUS_CODE_SUCCESS)
        self.assertEqual(decoded_ds["data"]["summaries"][0]["question"]["id"], 1)
        self.assertEqual(decoded_ds["data"]["summaries"][0]["answer"]["id"], 1)
        self.assertEqual(decoded_ds["data"]["hasMore"], True)

    def test_summary_question_2(self):
        user = User(username="Test1", password="123456")
        user.save()

        user2 = User(username="Test2", password="123456")
        user2.save()        

        question_list = []
        for i in range(3):
            question = Question()
            question.title = "This is question"
            question.owner = user   
            question.category = Category.objects.get(id=1)
            question.save()
            question_list.append(question)

        answer_list = []
        for i in range(2):
            data = {"detail" : "It is chicken"}

            path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(path, "testfiles/images.jpeg")
            
            upload_file = open(path, 'rb')
            file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
            
            form = AnswerForm(data, file_dic)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

            a = form.save(user=user, question=question_list[i])           
            answer_list.append(a)

        question_set = Question.objects.all()
        self.assertEqual(len(question_set), 3)        

        data = Question.to_summarize_question(question_set, user, 0, True)
        decoded_ds = json.loads(data)

        self.assertEqual(decoded_ds["status_code"], core.const.STATUS_CODE_SUCCESS)
        self.assertEqual(decoded_ds["data"]["summaries"][0]["question"]["id"], 1)
        self.assertEqual(decoded_ds["data"]["summaries"][0]["answer"]["id"], 1)
        self.assertEqual(decoded_ds["data"]["summaries"][1]["question"]["id"], 2)
        self.assertEqual(decoded_ds["data"]["summaries"][1]["answer"]["id"], 2)
        self.assertEqual(decoded_ds["data"]["hasMore"], True)

class AnswerTest(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

    def test_basic_answer(self):
        u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u1.save()
        self.assertEqual(User.objects.count(), 1)

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u1
        q.category = Category.objects.get(id=1)
        q.save()

        a = Answer()
        a.owner = u1
        a.question = q
        a.detail = "Cool!"
        
        a.image = 'testfiles/images.jpeg'
        a.save()

        client = Client()
        client.login(username="tester", password="12345")

        data = {"detail": "Very Cool!"}
        resp = client.post("/content/snapswer/update/" + str(a.id) + "/", data, follow=True)
        self.assertEqual(Answer.objects.get(id=1).detail, "Very Cool!")

    def test_vote_answer(self):
        u1 = User(username="tester", password="12345")
        u1.save()

        u2 = User(username="tester2", password="12345")
        u2.save()

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u1
        q.category = Category.objects.get(id=1)
        q.save()

        a = Answer()
        a.owner = u1
        a.question = q
        a.detail = "Cool!"
        
        a.image = 'testfiles/images.jpeg'
        a.save()

        a.votes.add(u1)
        self.assertEqual(a.votes.all().count(), 1)

        a.votes.add(u1)
        self.assertEqual(a.votes.all().count(), 1)

        a.votes.add(u2)
        self.assertEqual(a.votes.count(), 2)  

        self.assertEqual(u1.content_answer_votes.count(), 1)

    def test_latest_manager(self):
        u1 = User(username="tester", password="12345")
        u1.save()       

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u1
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(2):
            a = Answer()
            a.owner = u1
            a.question = q
            a.detail = "Cool!"

            a.image = 'testfiles/images.jpeg'
            a.save()

        self.assertEqual(Answer.latest.count(),2)

    def test_followable(self):
        u1 = User(username="tester", password="12345")
        u1.save()       

        u2 = User(username="tester2", password="12345")
        u2.save()  

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u1
        q.category = Category.objects.get(id=1)
        q.save()

        self.assertEqual(len(u2.content_question_followers.all()), 0)

        q.followers.add(u2)

        self.assertEqual(len(u2.content_question_followers.all()), 1)

    def test_to_detail_answer_response(self):
        u1 = User(username="tester", password="12345")
        u1.save()  

        u1 = User(username="tester2", password="12345")
        u1.save()          

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u1
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(10):
            data = {"detail" : "It is chicken"}

            path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(path, "testfiles/images.jpeg")
            
            upload_file = open(path, 'rb')
            file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
            
            form = AnswerForm(data, file_dic)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

            answer = form.save(user=u1, question=q)

        data = Answer.to_detail_answer_response(Answer.objects.all(), 120, u1, False)

        data = json.loads(data)

        self.assertEqual(len(data["data"]["answers"]), 10)        
        self.assertEqual(data["data"]["hasMore"], False)

class QuestionFormTest(TestCase):
    def test_basic_question_add(self):
        category = Category()
        category.name = "temp"
        category.save()

        u1 = User(username="tester", password="12345")
        u1.save()
        self.assertEqual(User.objects.count(), 1)

        data = {"title":"What is the best food in the world", 
                "detail": "Can anyone tell me?", "category": "1"}

        form = QuestionForm(data=data)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        question = form.save(u1)        
        self.assertEqual(question.owner , u1)
        self.assertEqual(question.title , "What is the best food in the world")
        self.assertEqual(question.detail , "Can anyone tell me?")

class AnswerFormTest(TestCase):

    path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(path, "testoutput")

    @override_settings(MEDIA_ROOT=path)
    def test_basic_answer_add(self):
        category = Category()
        category.name = "temp"
        category.save()

        u1 = User(username="tester", password="12345")
        u1.save()
        self.assertEqual(User.objects.count(), 1)

        data = {"title":"What is the best food in the world", 
                "detail": "Can anyone tell me?", "category": "1"}

        form = QuestionForm(data=data)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        question = form.save(u1)        
        self.assertEqual(question.owner , u1)
        self.assertEqual(question.title , "What is the best food in the world")
        self.assertEqual(question.detail , "Can anyone tell me?")

        data = {"detail" : "It is chicken"}

        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "testfiles/images.jpeg")
        
        upload_file = open(path, 'rb')
        file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
        
        form = AnswerForm(data, file_dic)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        answer = form.save(user=u1, question=question)
        self.assertEqual(answer.detail, "It is chicken")
        self.assertEqual(answer.question, question)
        self.assertEqual(answer.owner, u1)
        self.assertEqual((answer.image.name == "path"), False)


        query_set = Answer.objects.all()
        self.assertEqual(len(query_set), 1)
        data = Answer.to_profile_answer(query_set)
        data = json.loads(data)
        self.assertEqual(data["status_code"], core.const.STATUS_CODE_SUCCESS)
        self.assertEqual(data["data"]["answers"][0]["id"], 1)

class DisplayAPIViewTest(TestCase):
    def test_get_questions(self):
        category = Category()
        category.name = "temp"
        category.save()

        u1 = User(username="tester", password="12345")
        u1.save()
        self.assertEqual(User.objects.count(), 1)

        for i in range(11):
            data = {"title":"What is the best food in the world", 
                    "detail": "Can anyone tell me?", "category": "1"}

            form = QuestionForm(data=data)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

            question = form.save(u1)

        client = Client()
        resp = client.get('/content/display/',
                            data={"latestId": "9"},
                            follow=True)

        ret = json_string_to_list(resp.content)
        
        self.assertEqual(len(ret), 5)


        resp = client.get('/content/display/',
                            data={"latestId": "4"},
                            follow=True)        
        ret = json_string_to_list(resp.content)

        self.assertEqual(len(ret), 4)

        resp = client.get('/content/display/',
                            data={"latestId": "10"},
                            follow=True)        
        ret = json_string_to_list(resp.content)
        self.assertEqual(len(ret), 5)

class LoadItemViewTest(TestCase):
    def test_conver_data_in_json(self):
        category = Category()
        category.name = "temp"
        category.save()

        user = User(username="Test1", password="123456")
        user.save()

        user1 = User(username="Test2", password="123456")
        user1.save()

        for i in range(21):
            data = {"title": "What is it   str(i)?", "detail": "hi", "category": "1"}
            form = QuestionForm(data=data)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)
            question = form.save(user)         

            ans_data = {"detail" : "It is chicken"}
            path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(path, "testfiles/images.jpeg")
        
            upload_file = open(path, 'rb')
            file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
        
            form = AnswerForm(data, file_dic)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)
            answer = form.save(user=user1, question=question)
            
        qs = Question.objects.all()

        view = LoadQuestionView()
        data = view.convert_data_in_json(qs, 0)
        data = json.loads(data)

        self.assertEqual(len(data["questions"]), 21)
        self.assertEqual(data["questions"][0]["id"], 1)
        self.assertEqual(data["questions"][1]["id"], 2)

    #21 items and index will not increment after 21 items
    def test_indexing(self):
        category = Category()
        category.name = "temp"
        category.save()

        user = User(username="Test1", password="123456")
        user.save()

        user1 = User(username="Test2", password="123456")
        user1.save()

        for i in range(21):
            data = {"title": "What is ths best    str(i)?", "detail": "hi", "category" : "1"}
            form = QuestionForm(data=data)
            self.assertEqual(form.is_bound, True)
            self.assertEqual(form.is_valid(), True)

            question = form.save(user)         

        qs = Question.objects.all()       

        index = 0
        view = LoadQuestionView()
        data = view.convert_data_in_json(qs, index)
        data = json.loads(data)
        self.assertEqual(data["index"], 21)
        qs = Question.objects.filter(owner=user1)
        index = int(data["index"])

        data = view.convert_data_in_json(qs, index)
        data = json.loads(data)
        self.assertEqual(data["index"], 21)

class VoteAnswerViewTest(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

        self.u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        self.u1.save()

        self.u2 = User.objects.create_user("tester2", "tester2@snapswer.com", "12345")
        self.u2.save()

        self.question = Question()
        self.question.title = "Who is the best actor?"
        self.question.detail = "I would like to know who is the best actor"
        self.question.owner = self.u1
        self.question.category = Category.objects.get(id=1)
        self.question.save()

        self.answer = Answer()
        self.answer.owner = self.u1
        self.answer.question = self.question
        self.answer.detail = "Cool!"
        
        self.answer.image = 'testfiles/images.jpeg'
        self.answer.save()

    def test_vote(self):
        client = Client()
        ret = client.login(username="tester", password="12345")
        
        self.assertEqual(ret, True)

        resp = client.post('/content/vote/answer/' + str(self.answer.id) + "/")

        self.assertEqual(self.answer.votes.count(), 1)
        self.assertEqual(self.answer.votes.all()[0], self.u1)

    def test_unvote(self):
        client = Client()
        ret = client.login(username="tester", password="12345")
        
        self.assertEqual(ret, True)

        resp = client.post('/content/vote/answer/' + str(self.answer.id) + "/")

        self.assertEqual(self.answer.votes.count(), 1)
        self.assertEqual(self.answer.votes.all()[0], self.u1)

        # unvote
        resp = client.post('/content/vote/answer/' + str(self.answer.id) + "/")

        self.assertEqual(self.answer.votes.count(), 0)

class ContentSummaryViewTest(TestCase):
    def test_to_summary_struct(self):
        category = Category()
        category.name = "temp"
        category.save()

        user = User(username="Test1", password="123456")
        user.save()

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = user
        q.category = Category.objects.get(id=1)
        q.save()

        data = {"detail" : "It is chicken"}

        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "testfiles/images.jpeg")
        
        upload_file = open(path, 'rb')
        file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
        
        form = AnswerForm(data, file_dic)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        a = form.save(user=user, question=q)

        #data = ContentSummaryView.to_summary_struct(q, a)
        
        #self.assertEqual(data["q_id"], q.id)
        #self.assertEqual(data["a_id"], a.id)
    def test_category_view(self):
        category = Category()
        category.name = "temp"
        category.save()

        user = User(username="Test1", password="123456")
        user.save()


        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()

        user = User(username="Test2", password="123456")
        user.save()

        data = {"detail" : "It is chicken"}

        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "testfiles/images.jpeg")
        
        upload_file = open(path, 'rb')
        file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
        
        form = AnswerForm(data, file_dic)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        a = form.save(user=user, question=q)

        client = Client()
        url = "/content/summary/category/" + Category.objects.get(id=1).name + "/"
        data = {"index": 0}
        resp = client.get(url, data)

        data = json.loads(resp.content)
        self.assertEqual(len(data["data"]["summaries"]), 1)

class TestCacheManager(TestCase):
    def setUp(self):        
        category = Category()
        category.name = "temp"
        category.save()

    def tearDown(self):
        cache.clear()
            
    def test_best_answer_from_question_list(self):
        for i in range(1,11):
            username = "tester" + str(i)
            user = User(username=username, password="123456")
            user.save()

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(1,11):
            a = Answer()
            a.detail = str(i)
            a.owner = User.objects.get(id=i)
            a.question = q
            a.image = 'testfiles/images.jpeg'
            a.save()

        a1 = Answer.objects.get(id=1)
        a2 = Answer.objects.get(id=2)
        a3 = Answer.objects.get(id=3)
        a4 = Answer.objects.get(id=4)
        a5 = Answer.objects.get(id=5)
        a6 = Answer.objects.get(id=6)

        a1.votes.add(User.objects.get(id=1))

        a2.votes.add(User.objects.get(id=1))
        a2.votes.add(User.objects.get(id=2))

        a5.votes.add(User.objects.get(id=1))
        a5.votes.add(User.objects.get(id=2))
        a5.votes.add(User.objects.get(id=3))
        a5.votes.add(User.objects.get(id=4))

        data = ContentCacheManager.get_best_answer(q)
        self.assertEqual(data.detail, "5")

        a5.votes.remove(User.objects.get(id=1))
        a5.votes.remove(User.objects.get(id=2))
        a5.votes.remove(User.objects.get(id=3))
        a5.votes.remove(User.objects.get(id=4))

        # cache is not cleared yet so it is still 5
        data = ContentCacheManager.get_best_answer(q)
        self.assertEqual(data.detail, "5")

        cache.clear()

        data = ContentCacheManager.get_best_answer(q)
        self.assertEqual(data.detail, "2")

        # add a new question , test if the cache get updated or not
        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(1,11):
            a = Answer()
            a.detail = "new" + str(i)
            a.owner = User.objects.get(id=i)
            a.question = q
            a.image = 'testfiles/images.jpeg'
            a.save()

        new_a = Answer.objects.get(detail="new1")

        new_a.votes.add(User.objects.get(id=1))

        new_a2 = Answer.objects.get(detail="new2")

        new_a2.votes.add(User.objects.get(id=1))
        new_a2.votes.add(User.objects.get(id=2))
        new_a2.votes.add(User.objects.get(id=3))

        answer = ContentCacheManager.get_best_answer(q)

        self.assertEqual(answer, new_a2)

        cache_map = cache.get(const.CACHE_BEST_ANS_QUEST)

        self.assertEqual(cache_map.has_key(q), True)
        self.assertEqual(cache_map.get(q), answer)

    # test case 1: the cache is updated and questions are added
    def test_answer_cache_logic(self):
        ContentCacheManager.update_cache()

        for i in range(1,11):
            username = "tester" + str(i)
            user = User(username=username, password="123456")
            user.save()

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(1,11):
            a = Answer()
            a.detail = str(i)
            a.owner = User.objects.get(id=i)
            a.question = q
            a.image = 'testfiles/images.jpeg'
            a.save()

        a1 = Answer.objects.get(id=1)
        a2 = Answer.objects.get(id=2)
        a3 = Answer.objects.get(id=3)
        a4 = Answer.objects.get(id=4)
        a5 = Answer.objects.get(id=5)
        a6 = Answer.objects.get(id=6)

        a1.votes.add(User.objects.get(id=1))

        a2.votes.add(User.objects.get(id=1))
        a2.votes.add(User.objects.get(id=2))

        a5.votes.add(User.objects.get(id=1))
        a5.votes.add(User.objects.get(id=2))
        a5.votes.add(User.objects.get(id=3))
        a5.votes.add(User.objects.get(id=4))

        cache_map = cache.get(const.CACHE_BEST_ANS_QUEST)
        self.assertEqual(len(cache_map), 0)

        # call get_best_answer_from_question_list to make sure cache is updated
        ret_map = ContentCacheManager.get_best_answer_from_question_list()
        cache_map = cache.get(const.CACHE_BEST_ANS_QUEST)
        self.assertEqual(len(ret_map), len(cache_map))
        self.assertEqual(len(ret_map), 1)

        # now add a question and try to get best answer to make sure cache is updated
        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(1,11):
            a = Answer()
            a.detail = "new" + str(i)
            a.owner = User.objects.get(id=i)
            a.question = q
            a.image = 'testfiles/images.jpeg'
            a.save()

        ContentCacheManager.get_best_answer(q)
        cache_map = cache.get(const.CACHE_BEST_ANS_QUEST)
        self.assertEqual(len(cache_map), 2)
        self.assertEqual(cache_map.has_key(q), True)

        # must be the last answer since all votes a equal
        self.assertEqual(cache_map.get(q), a)

        # clear the cache and then try to get the best answer, the cache must be updated
        cache.clear()
        ContentCacheManager.get_best_answer(q)
        cache_map = cache.get(const.CACHE_BEST_ANS_QUEST)
        self.assertEqual(len(cache_map), 2)
        self.assertEqual(cache_map.has_key(q), True)     
        self.assertEqual(cache_map.get(q), a)   

class TestVoteSnapswerView(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        self.u1.save()

        self.u2 = User.objects.create_user("tester2", "tester2@snapswer.com", "12345")
        self.u2.save()

        category = Category()
        category.name = "temp"
        category.save()

    def test_post(self):
        client = Client()
        ret = client.login(username="tester2", password="12345")

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()

        for i in range(1,11):
            a = Answer()
            a.detail = "new" + str(i)
            a.owner = User.objects.get(id=1)
            a.question = q
            a.image = 'testfiles/images.jpeg'
            a.save()
        
        resp = client.post('/content/vote/answer/' + str(a.id) + "/",
                            follow=True)

        self.assertEqual(a.get_vote_count(), 1)

        resp = client.post('/content/vote/answer/' + str(a.id) + "/",
                            follow=True)

        self.assertEqual(a.get_vote_count(), 0)

        resp = client.post('/content/vote/answer/' + str(a.id) + "/",
                            follow=True)

        self.assertEqual(a.get_vote_count(), 1)      

        # test invalid
        resp = client.post('/content/vote/answer/999/',
                            follow=True)             

        self.assertEqual(a.get_vote_count(), 1)

    def test_post_question(self):
        client = Client()
        ret = client.login(username="tester2", password="12345")

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()      

        resp = client.post('/content/vote/question/' + str(q.id) + "/",
                            follow=True)      

        self.assertEqual(q.get_vote_count(), 1)

        resp = client.post('/content/vote/question/' + str(q.id) + "/",
                            follow=True)      

        self.assertEqual(q.get_vote_count(), 0)

        resp = client.post('/content/vote/question/' + str(q.id) + "/",
                            follow=True)      

        self.assertEqual(q.get_vote_count(), 1)

        # test invalid
        resp = client.post('/content/vote/question/999/',
                            follow=True)             

        self.assertEqual(q.get_vote_count(), 1)

class TestFollowQuestion(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        self.u1.save()

        self.u2 = User.objects.create_user("tester2", "tester2@snapswer.com", "12345")
        self.u2.save()

        category = Category()
        category.name = "temp"
        category.save()

    def test_follow_question(self):
        client = Client()
        ret = client.login(username="tester2", password="12345")

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = User.objects.get(id=1)
        q.category = Category.objects.get(id=1)
        q.save()      

        resp =  client.post('/content/follow/question/' + str(q.id) + "/",
                            follow=True)        
                            
        self.assertEqual(q.get_follow_count(), 1)       


        resp =  client.post('/content/follow/question/' + str(q.id) + "/",
                            follow=True)        
                            
        self.assertEqual(q.get_follow_count(), 0)

        resp =  client.post('/content/follow/question/' + str(q.id) + "/",
                            follow=True)        
                            
        self.assertEqual(q.get_follow_count(), 1)                           

        data = resp.content
        data = json.loads(data)

        self.assertEqual(data["data"]["action"], "follow")
        self.assertEqual(data["data"]["count"], 1)
        # test invalid

        resp =  client.post('/content/follow/question/999/',
                            follow=True)        
                            
        self.assertEqual(q.get_follow_count(), 1)     


class TestSeqAndIndex(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        self.u1.save()

        self.u2 = User.objects.create_user("tester2", "tester2@snapswer.com", "12345")
        self.u2.save()

        self.u3 = User.objects.create_user("tester3", "tester3@snapswer.com", "12345")
        self.u3.save()

        self.u4 = User.objects.create_user("tester4", "tester4@snapswer.com", "12345")
        self.u4.save()

        category = Category()
        category.name = "temp"
        category.save()

    @override_settings(USE_TZ=False)
    def test_question(self):
        for i in range(10):
            question = Question()
            question.title = "Question" + str(i)
            question.owner = self.u1
            question.category = Category.objects.get(id=1)
            question.save()

        time.sleep(1)

        timestamp = time.mktime(utcnow().timetuple())

        query_set , more = Question.get_by_seq_index(Question.objects.all(), 0, 4, timestamp)

        self.assertEqual(query_set.count(), 4)
        self.assertEqual(query_set[0].title, "Question9")
        self.assertEqual(query_set[1].title, "Question8")
        self.assertEqual(query_set[2].title, "Question7")
        self.assertEqual(query_set[3].title, "Question6")
        self.assertEqual(more, True)

        query_set , more = Question.get_by_seq_index(Question.objects.all(), 0, 6, timestamp)

        self.assertEqual(query_set.count(), 6)
        self.assertEqual(query_set[0].title, "Question9")
        self.assertEqual(query_set[1].title, "Question8")
        self.assertEqual(query_set[2].title, "Question7")
        self.assertEqual(query_set[3].title, "Question6")
        self.assertEqual(query_set[4].title, "Question5")
        self.assertEqual(query_set[5].title, "Question4")
        self.assertEqual(more, True)

        time.sleep(1)

        timestamp = time.mktime(utcnow().timetuple())

        query_set , more = Question.get_by_seq_index(Question.objects.all(), 0, 5, timestamp)
        self.assertEqual(query_set.count(), 5)

        self.assertEqual(query_set[0].title, "Question9")
        self.assertEqual(query_set[1].title, "Question8")
        self.assertEqual(query_set[2].title, "Question7")
        self.assertEqual(query_set[3].title, "Question6")
        self.assertEqual(query_set[4].title, "Question5")
        self.assertEqual(more, True)

        time.sleep(2)

        # add 2 more query old timestamp 
        for i in range(2):
            question = Question()
            question.title = "new Question" + str(i)
            question.owner = self.u1
            question.category = Category.objects.get(id=1)
            question.save()


        query_set , more = Question.get_by_seq_index(Question.objects.all(), 0, 5, timestamp)
        self.assertEqual(query_set.count(), 5)

        self.assertEqual(query_set[0].title, "Question9")
        self.assertEqual(query_set[1].title, "Question8")
        self.assertEqual(query_set[2].title, "Question7")
        self.assertEqual(query_set[3].title, "Question6")
        self.assertEqual(query_set[4].title, "Question5")
        self.assertEqual(more, True)

        time.sleep(1)
        timestamp = time.mktime(utcnow().timetuple())
        query_set , more = Question.get_by_seq_index(Question.objects.all(), 0, 5, timestamp)
        self.assertEqual(query_set.count(), 5)

        self.assertEqual(query_set[0].title, "new Question1")
        self.assertEqual(query_set[1].title, "new Question0")
        self.assertEqual(query_set[2].title, "Question9")
        self.assertEqual(query_set[3].title, "Question8")
        self.assertEqual(query_set[4].title, "Question7")        
        self.assertEqual(more, True)

        # test no timestamp
        query_set , more = Question.get_by_seq_index(Question.objects.all(), 0, 6)
        self.assertEqual(query_set.count(), 6)

        self.assertEqual(query_set[0].title, "new Question1")
        self.assertEqual(query_set[1].title, "new Question0")
        self.assertEqual(query_set[2].title, "Question9")
        self.assertEqual(query_set[3].title, "Question8")
        self.assertEqual(query_set[4].title, "Question7")  
        self.assertEqual(query_set[5].title, "Question6")    
        self.assertEqual(more, True)

    @override_settings(USE_TZ=False)
    def test_answer(self):
        question = Question()
        question.title = "Question"
        question.owner = self.u1
        question.category = Category.objects.get(id=1)
        question.save()

        for i in range(10):
            answer = Answer()
            answer.detail = "answer" + str(i)
            answer.owner = self.u1
            answer.question = question
            answer.save()

        time.sleep(1)

        timestamp = time.mktime(utcnow().timetuple())

        query_set, more = Answer.get_by_seq_index(Answer.objects.all(), 0, 4, timestamp)

        self.assertEqual(query_set.count(), 4)
        self.assertEqual(query_set[0].detail, "answer9")
        self.assertEqual(query_set[1].detail, "answer8")
        self.assertEqual(query_set[2].detail, "answer7")
        self.assertEqual(query_set[3].detail, "answer6")

        self.assertEqual(more, True)

        query_set , more = Answer.get_by_seq_index(Answer.objects.all(), 0, 6, timestamp)

        self.assertEqual(query_set.count(), 6)
        self.assertEqual(query_set[0].detail, "answer9")
        self.assertEqual(query_set[1].detail, "answer8")
        self.assertEqual(query_set[2].detail, "answer7")
        self.assertEqual(query_set[3].detail, "answer6")
        self.assertEqual(query_set[4].detail, "answer5")
        self.assertEqual(query_set[5].detail, "answer4")
        self.assertEqual(more, True)

        time.sleep(1)

        timestamp = time.mktime(utcnow().timetuple())

        query_set , more = Answer.get_by_seq_index(Answer.objects.all(), 0, 5, timestamp)
        self.assertEqual(query_set.count(), 5)

        self.assertEqual(query_set[0].detail, "answer9")
        self.assertEqual(query_set[1].detail, "answer8")
        self.assertEqual(query_set[2].detail, "answer7")
        self.assertEqual(query_set[3].detail, "answer6")
        self.assertEqual(query_set[4].detail, "answer5")
        self.assertEqual(more, True)

        time.sleep(2)

        # add 2 more query old timestamp 
        for i in range(2):
            answer = Answer()
            answer.detail = "new answer" + str(i)
            answer.owner = self.u1
            answer.question = question
            answer.save()

        query_set, more = Answer.get_by_seq_index(Answer.objects.all(), 0, 5, timestamp)
        self.assertEqual(query_set.count(), 5)

        self.assertEqual(query_set[0].detail, "answer9")
        self.assertEqual(query_set[1].detail, "answer8")
        self.assertEqual(query_set[2].detail, "answer7")
        self.assertEqual(query_set[3].detail, "answer6")
        self.assertEqual(query_set[4].detail, "answer5")
        self.assertEqual(more, True)

        time.sleep(1)
        timestamp = time.mktime(utcnow().timetuple())
        query_set , more = Answer.get_by_seq_index(Answer.objects.all(), 0, 5, timestamp)
        self.assertEqual(query_set.count(), 5)

        self.assertEqual(query_set[0].detail, "new answer1")
        self.assertEqual(query_set[1].detail, "new answer0")
        self.assertEqual(query_set[2].detail, "answer9")
        self.assertEqual(query_set[3].detail, "answer8")
        self.assertEqual(query_set[4].detail, "answer7")        
        self.assertEqual(more, True)

        # test no timestamp
        query_set, more = Answer.get_by_seq_index(Answer.objects.all(), 0, 6)
        self.assertEqual(query_set.count(), 6)

        self.assertEqual(query_set[0].detail, "new answer1")
        self.assertEqual(query_set[1].detail, "new answer0")
        self.assertEqual(query_set[2].detail, "answer9")
        self.assertEqual(query_set[3].detail, "answer8")
        self.assertEqual(query_set[4].detail, "answer7")  
        self.assertEqual(query_set[5].detail, "answer6")      
        self.assertEqual(more, True)


    @override_settings(USE_TZ=False)
    def test_regular_usage(self):
        for i in range(100):
            question = Question()
            question.title = "Question" + str(i)
            question.owner = self.u1
            question.category = Category.objects.get(id=1)
            question.save()

        # get 10 questions at a time, get 10 times        
        for i in range(9):
            query_set, more = Question.get_by_seq_index(Question.objects.all(), i * 10, 10)
            self.assertEqual(query_set.count(), 10)
            self.assertEqual(more, True)

        query_set, more = Question.get_by_seq_index(Question.objects.all(), 90, 10)
        self.assertEqual(query_set.count(), 10)
        self.assertEqual(more, False)

class TestCommentForm(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

    def test_save(self):
        u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u1.save()

        u2 = User.objects.create_user("tester2", "tester@snapswer.com", "12345")
        u2.save()
    
        question = Question()
        
        question.title = "Question"
        question.owner = u1
        question.category = Category.objects.get(id=1)
        question.save()

        answer = Answer()
        answer.detail = "new answer"
        answer.owner = u2
        answer.question = question
        answer.save()

        data = {"detail" : "It is chicken"}

        form = CommentForm(data)

        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        comment = form.save(user=u1, answer=answer)
        self.assertEqual(Comment.objects.all().count(), 1)

class TestCommentView(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

    def test_get(self):
        u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u1.save()

        u2 = User.objects.create_user("tester2", "tester@snapswer.com", "12345")
        u2.save()
    
        question = Question()
        
        question.title = "Question"
        question.owner = u1
        question.category = Category.objects.get(id=1)
        question.save()

        answer = Answer()
        answer.detail = "new answer"
        answer.owner = u2
        answer.question = question
        answer.save()

        data = {"detail" : "It is chicken"}

        form = CommentForm(data)

        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

        comment = form.save(user=u1, answer=answer)
        self.assertEqual(Comment.objects.all().count(), 1)        
 
        client = Client()
        resp = client.get('/content/comment/1', data={}, follow=True)
        
        resp = json.loads(resp.content)
        self.assertEqual(len(resp["data"]["comments"]), 1)
        self.assertEqual(resp["data"]["comments"][0]["owner"], "tester")

class TestCommentAddView(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

    def test_comment_add(self):
        u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u1.save()

        u2 = User.objects.create_user("tester2", "tester@snapswer.com", "12345")
        u2.save()
    
        question = Question()
        
        question.title = "Question"
        question.owner = u1
        question.category = Category.objects.get(id=1)
        question.save()

        answer = Answer()
        answer.detail = "new answer"
        answer.owner = u2
        answer.question = question
        answer.save()

        data = {"detail" : "this is a comment"}
        client = Client()
        ret = client.login(username="tester", password="12345")
        
        self.assertEqual(ret, True)

        resp = client.post('/content/comment/add/' + str(answer.id) + "/", data=data, 
                            follow=True)
        
        self.assertEqual(Comment.objects.all().count(), 1)  

        resp = json.loads(resp.content)
        self.assertEqual(resp["data"]["comment_count"], 1)      

class TestSnapswerDetailView(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

    def test_get(self):
        u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u1.save()

        u2 = User.objects.create_user("tester2", "tester@snapswer.com", "12345")
        u2.save()
        
        question = Question()
        
        question.title = "Question"
        question.owner = u1
        question.category = Category.objects.get(id=1)
        question.save()

        answer = Answer()
        answer.detail = "new answer"
        answer.owner = u2
        answer.question = question
        answer.save()

        comment = Comment()
        comment.detail = "Nice"
        comment.answer = answer
        comment.owner = u1
        comment.save()

        client = Client()
        resp = client.get('/content/snapswer/detail/1')

        resp = client.get('/content/snapswer/detail/100')

class TestCategory(SnapswerTestCase):
    def test_follow(self):
        user = User.objects.get(id=1)
        category = Category.objects.get(id=1)

        category.followers.add(user)

        self.assertEqual(category.followers.all().count(), 1)

        self.assertEqual(user.content_category_followers.all().count(), 1)

    def test_follow_question_user(self):
        user = User.objects.get(id=1)
        user_2 = User.objects.get(id=2)
        user_3 = User.objects.get(id=3)
        user_4 = User.objects.get(id=4)

        user_2.profile.followers.add(user)
        user_3.profile.followers.add(user)
        user_4.profile.followers.add(user)

        question1 = Question()
        question1.title = "Question"
        question1.owner = user_2
        question1.category = Category.objects.get(id=1)
        question1.save()

        question2 = Question()
        question2.title = "Question"
        question2.owner = user_3
        question2.category = Category.objects.get(id=2)
        question2.save()

        question3 = Question()
        question3.title = "Question"
        question3.owner = user_4
        question3.category = Category.objects.get(id=3)
        question3.save()        

        questions = Question.get_follow_questions(user)

        self.assertEqual(questions.count(), 3)

    def test_follow_question_only(self):
        user = User.objects.get(id=1)
        user_2 = User.objects.get(id=2)
        user_3 = User.objects.get(id=3)
        user_4 = User.objects.get(id=4)

        question1 = Question()
        question1.title = "Question"
        question1.owner = user_2
        question1.category = Category.objects.get(id=1)
        question1.save()

        question1.followers.add(user)

        question2 = Question()
        question2.title = "Question"
        question2.owner = user_3
        question2.category = Category.objects.get(id=2)
        question2.save()

        question3 = Question()
        question3.title = "Question"
        question3.owner = user_4
        question3.category = Category.objects.get(id=3)
        question3.save()        

        questions = Question.get_follow_questions(user)

        self.assertEqual(questions.count(), 1)

    def test_follow_question_category(self):
        user = User.objects.get(id=1)
        user_2 = User.objects.get(id=2)
        user_3 = User.objects.get(id=3)
        user_4 = User.objects.get(id=4)

        question1 = Question()
        question1.title = "Question"
        question1.owner = user_2
        question1.category = Category.objects.get(id=1)
        question1.save()

        Category.objects.get(id=1).followers.add(user)

        question2 = Question()
        question2.title = "Question"
        question2.owner = user_3
        question2.category = Category.objects.get(id=2)
        question2.save()

        Category.objects.get(id=2).followers.add(user)

        question3 = Question()
        question3.title = "Question"
        question3.owner = user_4
        question3.category = Category.objects.get(id=3)
        question3.save()        

        questions = Question.get_follow_questions(user)

        self.assertEqual(questions.count(), 2)


    def test_all_follow_question(self):
        current_user = User.objects.get(id=1)

        follow_user_1 = User.objects.get(id=2)
        follow_user_2 = User.objects.get(id=3)
        follow_user_3 = User.objects.get(id=4)

        question1 = Question()
        question1.title = "Question"
        question1.owner = follow_user_1
        question1.category = Category.objects.get(id=1)
        question1.save()

        question2 = Question()
        question2.title = "Question"
        question2.owner = follow_user_2
        question2.category = Category.objects.get(id=2)
        question2.save()

        question3 = Question()
        question3.title = "Question"
        question3.owner = follow_user_3
        question3.category = Category.objects.get(id=3)
        question3.save()

        questions = Question.get_follow_questions(current_user)

        self.assertEqual(questions, None)

        question1.followers.add(current_user)
        question2.followers.add(current_user)
        question3.followers.add(current_user)

        questions = Question.get_follow_questions(current_user)
        self.assertEqual(len(questions), 3)

        # now current_user follow category 1, expect only 3 questions returns
        cat = Category.objects.get(id=1)
        cat.followers.add(current_user)

        questions = Question.get_follow_questions(current_user)
        self.assertEqual(len(questions), 3)

        # now current_user follow category 4 which has one question, expect 4 questions
        question4 = Question()
        question4.title = "Question"
        question4.owner = User.objects.get(id=5)
        question4.category = Category.objects.get(id=4)
        question4.save()      

        cat = Category.objects.get(id=4)
        cat.followers.add(current_user)

        questions = Question.get_follow_questions(current_user)
        self.assertEqual(len(questions), 4)

        # now current_user follow user4 who has added two questions, expect 6 questions
        user4 = User.objects.get(id=5)
        user4.profile.followers.add(current_user)

        question4 = Question()
        question4.title = "Question"
        question4.owner = user4
        question4.category = Category.objects.get(id=4)
        question4.save()      

        question4 = Question()
        question4.title = "Question"
        question4.owner = user4
        question4.category = Category.objects.get(id=4)
        question4.save()   

        questions = Question.get_follow_questions(current_user)
        self.assertEqual(len(questions), 6)

        # now current_user follow the rest of the users, expect only 6 questions
        follow_user_1.profile.followers.add(current_user)
        follow_user_2.profile.followers.add(current_user)
        follow_user_3.profile.followers.add(current_user)

        questions = Question.get_follow_questions(current_user)
        self.assertEqual(len(questions), 6)        

class TestFollowCategory(SnapswerTestCase):
    def test_follow_category(self):
        user = User.objects.get(id=1)
        user.set_password('1234')
        user.save()
        cat = Category.objects.get(id=1)

        client = Client()
        client.login(username='mockuser0', password='1234')

        client.post('/content/follow/category/1/', follow=True)

        self.assertEqual(cat.followers.count(), 1)

class TestQuestionDay(SnapswerTestCase):
    def test_basic(self):
        user = User.objects.get(id=1)
        question = Question()
        question.owner = user
        question.title = "1"
        question.category = Category.objects.get(id=1)
        question.save()

        question = Question()
        question.owner = user
        question.title = "2"
        question.category = Category.objects.get(id=1)
        question.save()

        client = Client()
        data = client.get('/content/question_of_the_day/', follow=True)

class TestDeleteView(SnapswerTestCase):
    def test_delete_question(self):
        user = User.objects.get(id=1)
        question = Question()
        question.owner = user
        question.title = "1"
        question.category = Category.objects.get(id=1)
        question.save()

        self.assertEqual(Question.delete_question(1, user), True)

    def test_delete_snapswer(self):
        user = User.objects.get(id=1)
        question = Question()
        question.owner = user
        question.title = "1"
        question.category = Category.objects.get(id=1)
        question.save()        

        a = Answer()
        a.owner = user
        a.question = question
        a.detail = "Cool!"
        
        a.image = 'testfiles/images.jpeg'
        a.save()

        self.assertEqual(Answer.delete_snapswer(1, user), True)

    def test_delete_using_view(self):
        user = User.objects.get(id=1)
        question = Question()
        question.owner = user
        question.title = "1"
        question.category = Category.objects.get(id=1)
        question.save()        

        a = Answer()
        a.owner = user
        a.question = question
        a.detail = "Cool!"
        
        a.image = 'testfiles/images.jpeg'
        a.save()

        client = Client()
        data = client.get('/content/question/delete/1', follow=True)

        self.assertEqual(Question.delete_question(1, user), True)      

        data = client.get('/content/answer/delete/1', follow=True)

        self.assertEqual(Answer.delete_snapswer(1, user), True)
     