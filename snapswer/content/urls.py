from django.conf.urls import patterns, url
import views
import core.views

urlpatterns = patterns('',
	url(r'^ask/$', views.AskView.as_view(), name="ask"),
	url(r'^loaditem/$', views.LoadQuestionView.as_view(), name="loaditem"),
	url(r'^board/(\d+)/$', views.BoardView.as_view(), name="board"),
	url(r'^vote/answer/(\d+)/$', views.VoteContentView.as_view(content_type="answer"), name="vote_answer"),
	url(r'^vote/question/(\d+)/$', views.VoteContentView.as_view(content_type="question"), name="vote_question"),
	url(r'^summary/(?P<summary_type>\w+)/$', views.ContentSummaryView.as_view(), name="content_summary"),
	url(r'^summary/(?P<summary_type>\w+)/(?P<category>\w+)/$', views.ContentSummaryView.as_view(), name="content_summary_category"),
	url(r'^follow/question/(\d+)/$', core.views.FollowView.as_view(follow_type="question"), name="follow_question"),
	url(r'^comment/(?P<answer_id>\d+)/$', views.CommentView.as_view(), name="comment"),
	url(r'^comment/add/(?P<answer_id>\d+)/$', views.CommentAddView.as_view(), name="comment_add"),
	url(r'^snapswer/detail/(?P<answer_id>\d+)/$', views.SnapswerDetailView.as_view(), name="snapswer_detail"),
	url(r'^follow/category/(\d+)/$', core.views.FollowView.as_view(follow_type="category"), name="follow_category"),
	url(r'^snapswer/update/(?P<answer_id>\d+)/$', views.UpdateSnapswerView.as_view(), name="snapswer_update"),
	url(r'^question/update/(?P<question_id>\d+)/$', views.UpdateQuestionView.as_view(), name="question_update"),
	# API Views
	url(r'^display/$', views.DisplayAPIView.as_view(), name="display"),
	url(r'^snapswer/(\d+)/$', views.AddSnapswerView.as_view(), name="snapswer"),
	url(r'^loadsnapswer/(\d+)/$', views.LoadSnapswerView.as_view(), name="loadsnapswer"),
	url(r'^snapswer/emailshare/(\d+)/$', views.EmailShareView.as_view(share_type="snapswer"), name="email_answer"),
	url(r'^question/emailshare/(\d+)/$', views.EmailShareView.as_view(share_type="question"), name="email_question"),
	url(r'^question_of_the_day/$', views.QuestionOfDay.as_view(), name="question_of_the_day"),
	url(r'^snapswer/delete/(\d+)/$', core.views.DeleteView.as_view(content_type="snapswer"), name="snapswer_delete"),
	url(r'^question/delete/(\d+)/$', core.views.DeleteView.as_view(content_type="question"), name="question_delete"),
)