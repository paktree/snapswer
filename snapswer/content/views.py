# Create your views here.

from django.views.generic.base import View
from .forms import QuestionForm, AnswerForm, AnswerUrlForm, CommentForm
from django.conf import settings
from django.shortcuts import redirect, render
from .models import Question, Answer, Comment, Category
from django.core.paginator import Paginator
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
import const
from core.utils import SnapswerJsonEncoder, to_unsuccess_empty_response, to_success_empty_response, to_require_login_response
import json
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .utils import ContentCacheManager
from core.models import VoteableModel
from django.http import Http404
from django.contrib.auth.models import User
from core.utils import snapswerresponse
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
import core.const
import logging
from django.templatetags.static import static

from datetime import datetime

class AskView(View):

    @staticmethod
    @snapswerresponse
    def to_ask_success_response():
        data = {}
        return data, core.const.STATUS_CODE_SUCCESS

    @staticmethod
    @snapswerresponse
    def to_ask_fail_response(error_msg):
        data = {"error_msg": error_msg}
        return data, core.const.STATUS_CODE_UNSUCCESS

    def post(self, request):
        form = QuestionForm(data=request.POST)
        if form.is_valid():
            form.save(request.user)
            data = AskView.to_ask_success_response()

            return HttpResponse(data, mimetype="json")
        else:
            data = AskView.to_ask_fail_response(form.errors)

            return HttpResponse(data, mimetype="json")

class LoadQuestionView(View):
    def convert_data_in_json(self, query_set, index):
        x = 0
        data = []
        for question in query_set:
            info = {}
            info["id"] = question.id
            info["title"] = question.title
            info["created"] = question.created
            info["owner"] = question.owner.username
            info["detail"] = question.detail
            info["answerCount"] = question.answer_set.count()
            answers = question.answer_set.all()[:5]
            pic = []
            for answer in answers:
                pic_info = {}
                pic_info["source"] = answer.get_thumbnail(const.THUMBNAIL_60_60_OPTION).url
                pic.append(pic_info)
                info["pic"] = pic

            data.append(info)
            
        response_data = {}
        response_data["questions"] = data
        response_data["index"] = index + len(query_set)
        data = json.dumps(response_data, cls=SnapswerJsonEncoder)
        
        return data
    
    def get(self, request):
        timestamp = request.GET['seq_num']
        index = request.GET['index']
        
        index = int(index)

        qs , more = Question.get_by_seq_index(Question.objects.all(), index, const.ITEM_PER_LOAD, timestamp)

        if (len(qs) == 0):
            return HttpResponseBadRequest(mimetype="json")

        data = self.convert_data_in_json(qs, index)
        return HttpResponse(data, mimetype="json")

class DisplayAPIView(APIView):
    NUM_RECORD = 5

    def get(self, request, format=None):
        latest_id = request.QUERY_PARAMS['latestId']
        qs = Question.objects.order_by('-created').filter(id__lte=latest_id)[:self.NUM_RECORD]

        json_data_list = serializers.serialize('json', qs)
        return Response(json_data_list, status=status.HTTP_200_OK)

class BoardView(View):
    def get(self, request, question_id):
        questions = Question.objects.filter(id=question_id)


        if questions:
            question_followed = False
            category_followed = False

            if request.user:
                question_followed = questions[0].is_followed(request.user)
                category_followed = questions[0].category.is_followed(request.user)

            context = {
                "question" : questions[0],
                "currentUrl" : request.get_full_path(),
                "question_followed" : question_followed,
                "category_followed" : category_followed
            }

            return render(request, 'content/detail.html', context)
        else:
            raise Http404

class LoadSnapswerView(View):
    def get(self, request, question_id):
        questions = Question.objects.filter(id=question_id)

        if questions.exists():
            index = request.GET['index']
            
            index = int(index)
            if ('seq_num' in request.GET):
                timestamp = request.GET['seq_num']
                answer_objs , more = Answer.get_by_seq_index(questions[0].answer_set, index, const.SNAPSWER_PER_LOAD, timestamp)
            else:
                answer_objs , more = Answer.get_by_seq_index(questions[0].answer_set, index, const.SNAPSWER_PER_LOAD)

            index = index + answer_objs.count()
            data = Answer.to_detail_answer_response(answer_objs, index, request.user, more)

            return HttpResponse(data, mimetype="json")
        else:
            raise Http404

class AddSnapswerView(View):

    @staticmethod
    @snapswerresponse
    def to_snapswer_success_response():
        data = {}
        return data, core.const.STATUS_CODE_SUCCESS

    @staticmethod
    @snapswerresponse
    def to_snapswer_fail_response(error_msg):
        data = {"error_msg": error_msg}
        return data, core.const.STATUS_CODE_UNSUCCESS

    def post(self, request, question_id):
        if 'image_url' in request.POST:
            form = AnswerUrlForm(request.POST)
            if form.is_valid():
                questions = Question.objects.filter(id=question_id)
                if questions.exists():
                    answer = form.save(request.user, questions[0])
                    data = AddSnapswerView.to_snapswer_success_response()
                    return HttpResponse(data, mimetype="json")
                
                data = AddSnapswerView.to_snapswer_fail_response("")
                return HttpResponse(data, mimetype="json")
            else:
                data = AddSnapswerView.to_snapswer_fail_response(form.errors)
                return HttpResponse(data, mimetype="json")
        else:
            form = AnswerForm(request.POST, request.FILES)
            if form.is_valid():
                questions = Question.objects.filter(id=question_id)
                if questions.exists():
                    form.save(request.user, questions[0])
                    data = AddSnapswerView.to_snapswer_success_response()
                    return redirect(request.GET['next'])
                else:
                    data = AddSnapswerView.to_snapswer_fail_response("")
                    raise Http404
            else:
                data = AddSnapswerView.to_snapswer_fail_response(form.errors)
                raise Http404

class UpdateSnapswerView(View):

    @method_decorator(login_required)
    def post(self, request, answer_id):
        try:
            answer = Answer.objects.get(id=answer_id)
            if answer.owner != request.user:
                data = to_unsuccess_empty_response()
                return HttpResponse(data, mimetype="json")

            if 'detail' in request.POST:
                answer.detail = request.POST['detail']
                answer.save()

                data = Answer.to_detail_answer_response([answer], 0, request.user, False)
                return HttpResponse(data, mimetype="json")

            else:
                data = to_unsuccess_empty_response()
                return HttpResponse(data, mimetype="json")

        except Answer.DoesNotExist:
            raise Http404

class UpdateQuestionView(View):
    @method_decorator(login_required)
    def post(self, request, question_id):
        try:
            question = Question.objects.get(id=question_id)
            if question.owner != request.user:
                data = to_unsuccess_empty_response()
                return HttpResponse(data, mimetype="json")

            if 'detail' in request.POST:
                question.detail = request.POST['detail']

            if 'category' in request.POST:
                cat_id = request.POST['category']
                try:
                    cat = Category.objects.get(id=cat_id)
                    question.category = cat

                except Category.DoesNotExist:
                    data = to_unsuccess_empty_response()
                    return HttpResponse(data, mimetype="json")
                    
            if 'title' in request.POST:
                question.title = request.POST['title']

            question.save()

            data = Question.to_summarize_question([question], request.user, 0, False)

            return HttpResponse(data, mimetype="json")
        except Question.DoesNotExist:
            raise Http404

class VoteContentView(View):
    content_type = None

    def post(self, request, id):
        if not request.user.is_authenticated():
            data = to_require_login_response()
            return HttpResponse(data, mimetype="json")

        obj = None
        if self.content_type == 'answer':
            obj = Answer.objects.filter(id=id)
        elif self.content_type == "question":
            obj = Question.objects.filter(id=id)
        
        if obj.count() == 1:
            obj = obj[0]

            user = obj.votes.filter(username=request.user.username)
            if user:
                # if user exist, unvote it
                obj.remove_vote(request.user)
                data = VoteableModel.to_vote_response(obj, False)
            else:
                # if user doesn't exist, vote it
                obj.add_vote(request.user)
                data = VoteableModel.to_vote_response(obj, True)
        else:
            data = to_unsuccess_empty_response()

        return HttpResponse(data, mimetype="json")

class ContentSummaryView(View):

    def get(self, request, summary_type, category=None):
        logger = logging.getLogger('content')
        logger.info("Request summary")
        index = request.GET['index']
        index = int(index)

        timestamp = None
        if ('seq_num' in request.GET):
            timestamp = request.GET['seq_num']        

        data = {}
        if summary_type == const.SUMMARY_TYPE_LATEST:
        
            questions, more = Question.get_by_seq_index(Question.objects.all(), index, const.QUESTION_PER_LOAD, timestamp)

            new_index = questions.count() + index

            data = Question.to_summarize_question(questions, request.user, new_index, more)

        elif summary_type == const.SUMMARY_TYPE_POPULAR:
        
            questions = []
            questions_weight_map = ContentCacheManager.get_most_weighted_question()
        
            for question, weight in questions_weight_map:
                questions.append(question)
        
            data = Question.to_summarize_question(questions, request.user)
        
        elif summary_type == const.SUMMARY_TYPE_FOLLOW:

            questions = Question.get_follow_questions(request.user)

            if questions:
                questions , more = Question.get_by_seq_index(questions, index, const.QUESTION_PER_LOAD, timestamp)
            
                new_index = questions.count() + index

                data = Question.to_summarize_question(questions, request.user, new_index, more)
            else:
                data = to_success_empty_response()

        elif summary_type == const.SUMMARY_TYPE_CATEGORY:

            cat_obj = Category.get_obj_from_name(category)

            if not cat_obj:
                raise Http404

            questions = Question.objects.filter(category=cat_obj)
            question, more = Question.get_by_seq_index(questions, index, const.QUESTION_PER_LOAD, timestamp)
            new_index = questions.count() + index

            data = Question.to_summarize_question(questions, request.user, new_index, more)
        else:
            raise Http404
            
        logger.info("Request summary done")
        return HttpResponse(data, mimetype="json")

class CommentView(View):
    def get(self, request, answer_id):
        answer = Answer.objects.filter(id=answer_id)

        if not answer:
            raise Http404

        if answer.count() != 1:
            raise Http404

        answer = answer[0]
        comment_set = answer.comment_set.all()
        data = Comment.to_comment_response(comment_set)

        return HttpResponse(data, mimetype="json")

class CommentAddView(View):
    @method_decorator(login_required)
    def post(self, request, answer_id):
        form = CommentForm(data=request.POST)

        answer = Answer.objects.filter(id=answer_id)
        if not answer:
            raise Http404

        if answer.count() != 1:
            raise Http404

        answer = answer[0]

        if form.is_valid():
            form.save(user=request.user, answer=answer)
            data = Comment.to_add_success(answer)
            return HttpResponse(data, mimetype="json")
        else:
            data = Comment.to_add_fail()
            return HttpResponse(data, mimetype="json")

class SnapswerDetailView(View):
    def get(self, request, answer_id):
        try:
            answer = Answer.objects.select_related().prefetch_related('comment_set').get(id=answer_id)
            context = {}

            context["answer"] = answer
            context["liked"] = False
            context["followed"] = False

            if request.user:
                user_id = request.user.id
                try:
                    user = answer.votes.get(id=user_id)
                    context["liked"] = True
                except User.DoesNotExist:
                    pass

                try:
                    answer_user = answer.owner
                    user = answer_user.profile.followers.get(id=user_id)
                    context["followed"] = True
                except User.DoesNotExist:
                    pass


            context["image"] = answer.get_thumbnail(const.THUMBNAIL_300_0_OPTION)
            context["comments"] = answer.comment_set.all()
            return render(request, 'content/snapswer_detail.html', context)

        except Answer.DoesNotExist:
            raise Http404

class EmailShareView(View):
    share_type = None

    @staticmethod
    def share_content(question, user, target):

        subject = user.username + " would like to share \"" + question.title.rstrip() + "\" with you"

        htmly = get_template('content/share_email.html')
        
        d = Context({"question_id": question.id, 
                    "title": subject})

        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, html_content, 'mail-noreply@snapswer.com', [target])
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def share_snapswer(request, question, snapswer, user, target):

        subject = user.username + " just shared a Snapswer with you"

        html = get_template('content/share_snapswer_email.html')

        board_url = request.get_full_path()
        thumbnail = snapswer.get_thumbnail(const.THUMBNAIL_300_0_OPTION) 
        thumb_url = thumbnail.url
        template_data = Context({"question_id": question.id,
                     "title": question.title,
                     "snapswer_img": thumb_url,
                     "img_width": thumbnail.width,
                     "img_height": thumbnail.height,
                     "home": "http://"+request.META["HTTP_HOST"]})

        html_content = html.render(template_data)
        msg = EmailMultiAlternatives(subject, html_content, 'mail-noreply@snapswer.com', [target])
        msg.content_subtype = "html"
        msg.send()

    @method_decorator(login_required)
    def post(self, request, id):
        email = request.POST["email"]
        email_list = email.split(',')

        sent_list = []
        for individual_email in email_list :

            if individual_email in sent_list:
                continue

            if self.share_type == "snapswer":
                try:
                    answer = Answer.objects.select_related().get(id=id)
                    question = answer.question
                    EmailShareView.share_snapswer(request, question, answer, request.user, individual_email)
                except Answer.DoesNotExist:
                    data = to_unsuccess_empty_response()
                    return HttpResponse(data, mimetype="json")

            elif self.share_type == "question":
                try:
                    question = Question.objects.get(id=id)
                    EmailShareView.share_content(question, request.user, individual_email)
                except Question.DoesNotExist:
                    data = to_unsuccess_empty_response()
                    return HttpResponse(data, mimetype="json")

            sent_list.append(individual_email)

        data = to_success_empty_response()
        return HttpResponse(data, mimetype="json")

class QuestionOfDay(View):
    def get(self, request):
        question = Question.objects.latest('created')

        if question:
            question_followed = False
            category_followed = False

            if request.user:
                question_followed = question.is_followed(request.user)
                category_followed = question.category.is_followed(request.user)

            context = {
                "question" : question,
                "currentUrl" : request.get_full_path(),
                "question_followed" : question_followed,
                "category_followed" : category_followed
            }

            return render(request, 'content/detail.html', context)
        else:
            raise Http404