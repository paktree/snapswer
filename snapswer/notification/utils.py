class EventMessageFactory(object):
	def get_event_message(self, event):
		import notification.const
		if event.event_type == notification.const.EVENT_ANSWER_ADD:
			user_str = ''

			if event.triggers.count() >= 1:
				user_str = event.triggers.all()[0].username

				more = False
				if event.triggers.count() > 5:
					more = True

				for user in event.triggers.all()[1:5]:
					user_str = user_str + ", " + user.username

				if more:
					user_str = user_str + " and more"

				msg = user_str + " just add snapswers on \"" + event.question.title + "\""
				return msg
			else:
				return ''