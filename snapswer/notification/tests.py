"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from .models import BaseEvent
from django.contrib.auth.models import User
from content.models import Question, Answer, Category
from .utils import EventMessageFactory
from django.test.client import Client
import json

class EventTest(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()

    def test_event_add(self):
        u = User(username="tester", password="12345")
        u.save()
        self.assertEqual(User.objects.count(), 1)

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u
        q.category = Category.objects.get(id=1)
        q.save()

        a = Answer()
        a.owner = User.objects.get(id=1)
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()

        event = BaseEvent()
        event.answer = a
        event.question = q
        event.owner = u
        event.event_type = 'ANSWER_ADD'
        event.save()

        event = BaseEvent()
        event.answer = a
        event.question = q
        event.owner = u
        event.event_type = 'ANSWER_ADD'
        event.save()

        self.assertEqual(BaseEvent.objects.count(), 3)
        self.assertEqual(BaseEvent.objects.all()[0].triggers.count(), 1)
        factory = EventMessageFactory()
        resp = BaseEvent.to_event_response(BaseEvent.objects.all(), u)

        decoded_ds = json.loads(resp)
        
        self.assertEqual(len(decoded_ds["data"]["events"]), 3)

    def test_no_event(self):
        u = User(username="tester", password="12345")
        u.save()
        self.assertEqual(User.objects.count(), 1)

        resp = BaseEvent.to_event_response(BaseEvent.objects.all(), u)
        decoded_ds = json.loads(resp)
        self.assertEqual(len(decoded_ds["data"]["events"]), 0)

    def test_view(self):
        u = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u.save()
        
        client = Client()

        client.login(username="tester", password="12345")
        
        resp = client.get('/notification/getlatest/', data={}, follow=True)
        resp = json.loads(resp.content)

        self.assertEqual(len(resp["data"]["events"]), 0)

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u
        q.category = Category.objects.get(id=1)
        q.save()

        a = Answer()
        a.owner = User.objects.get(id=1)
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()

        for i in range(10):
            event = BaseEvent()
            event.answer = a
            event.question = q
            event.owner = u
            event.event_type = 'ANSWER_ADD'
            event.save()

        resp = client.get('/notification/getlatest/', data={}, follow=True)
        resp = json.loads(resp.content)

        self.assertEqual(len(resp["data"]["events"]), 5)
        self.assertEqual(resp["data"]["new_count"], 11)

        # mark first 3 as read
        event_1 = BaseEvent.objects.get(id=1)
        event_1.acknowledged = True
        event_1.save()

        event_2 = BaseEvent.objects.get(id=2)
        event_2.acknowledged = True
        event_2.save()

        event_3 = BaseEvent.objects.get(id=3)
        event_3.acknowledged = True
        event_3.save()

        resp = client.get('/notification/getlatest/', data={}, follow=True)
        resp = json.loads(resp.content)

        self.assertEqual(len(resp["data"]["events"]), 5)
        self.assertEqual(resp["data"]["new_count"], 8)        

        # add 3 more event
        for i in range(3):
            event = BaseEvent()
            event.answer = a
            event.question = q
            event.owner = u
            event.event_type = 'ANSWER_ADD'
            event.save()        

        resp = client.get('/notification/getlatest/', data={}, follow=True)
        resp = json.loads(resp.content)

        # expect 10 returns
        self.assertEqual(len(resp["data"]["events"]), 5)
        self.assertEqual(resp["data"]["new_count"], 11)               

    def test_trigger_count(self):
        u = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u.save()

        u1 = User.objects.create_user("tester1", "tester@snapswer.com", "12345")
        u1.save()        

        u2 = User.objects.create_user("tester2", "tester@snapswer.com", "12345")
        u2.save()        

        u3 = User.objects.create_user("tester3", "tester@snapswer.com", "12345")
        u3.save()   

        u4 = User.objects.create_user("tester4", "tester@snapswer.com", "12345")
        u4.save()        

        u5 = User.objects.create_user("tester5", "tester@snapswer.com", "12345")
        u5.save()   

        u6 = User.objects.create_user("tester6", "tester@snapswer.com", "12345")
        u6.save()        

        u7 = User.objects.create_user("tester7", "tester@snapswer.com", "12345")
        u7.save()   

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u
        q.category = Category.objects.get(id=1)
        q.save()

        a = Answer()
        a.owner = u1
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()

        a = Answer()
        a.owner = u2
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()        

        a = Answer()
        a.owner = u3
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()        

        a = Answer()
        a.owner = u4
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()   

        a = Answer()
        a.owner = u5
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()   

        a = Answer()
        a.owner = u6
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()   

        self.assertEqual(BaseEvent.objects.all().count(), 1)
        self.assertEqual(BaseEvent.objects.all()[0].triggers.count(), 6)


        # mark it as ack
        event = BaseEvent.objects.all()[0]
        event.acknowledged = True
        event.save()

        factory = EventMessageFactory()
        ret = factory.get_event_message(event)
        self.assertNotEqual(len(ret), 0)

        a = Answer()
        a.owner = u2
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()   

        self.assertEqual(BaseEvent.objects.all().count(), 2)
        self.assertEqual(BaseEvent.objects.all()[1].triggers.count(), 1)

        event = BaseEvent.objects.all()[1]   
        ret = factory.get_event_message(event)

        self.assertNotEqual(len(ret), 0)

class TestReadView(TestCase):
    def setUp(self):
        category = Category()
        category.name = "temp"
        category.save()
        
    def test_get(self):
        u = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u.save()

        u1 = User.objects.create_user("tester1", "tester@snapswer.com", "12345")
        u1.save()        

        u2 = User.objects.create_user("tester2", "tester@snapswer.com", "12345")
        u2.save()        

        q = Question()
        q.title = "Who is the best actor?"
        q.detail = "I would like to know who is the best actor"
        q.owner = u
        q.category = Category.objects.get(id=1)
        q.save()

        a = Answer()
        a.owner = u1
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()

        a = Answer()
        a.owner = u2
        a.question = q
        a.image = 'testfiles/images.jpeg'
        a.save()        

        self.assertEqual(BaseEvent.objects.all().count(), 1)
        self.assertEqual(BaseEvent.objects.all()[0].acknowledged, False)

        client2 = Client()
        client2.login(username="tester2", password="12345")
        # shouldn't get updated
        ret = client2.get('/notification/acknowledge', data={}, follow=True)
        
        self.assertEqual(BaseEvent.objects.all()[0].acknowledged, False)


        client = Client()
        client.login(username="tester", password="12345")
        ret = client.get('/notification/acknowledge/', data={}, follow=True)

        self.assertEqual(BaseEvent.objects.all().count(), 1)
        self.assertEqual(BaseEvent.objects.all()[0].acknowledged, True)
