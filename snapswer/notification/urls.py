from django.conf.urls import patterns, url
from .views import LatestView, AcknowledgeView, NotificationView

urlpatterns = patterns('',
	url(r'', NotificationView.as_view(), name="notification"),
	url(r'getlatest/$', LatestView.as_view(), name="get_latest"),
	url(r'acknowledge/$', AcknowledgeView.as_view(), name="read")
)