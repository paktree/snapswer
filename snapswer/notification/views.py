# Create your views here.
from django.views.generic.base import View
from .models import BaseEvent
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from core.utils import to_unsuccess_empty_response
from django.shortcuts import render
from .utils import EventMessageFactory

class LatestView(View):
	@method_decorator(login_required)
	def get(self, request):
		# get latest 5
		events = BaseEvent.get_latest(request.user)
		data = BaseEvent.to_event_response(events, request.user)

		return HttpResponse(data, mimetype="json")

class AcknowledgeView(View):
	@method_decorator(login_required)
	def get(self, request):
		event_set = BaseEvent.objects.filter(owner=request.user, acknowledged=False)

		for event in event_set:
			if event:
				event.acknowledged = True
				event.save()

		data = BaseEvent.to_event_read_response()
		return HttpResponse(data, mimetype="json")
	
class NotificationView(View):
	@method_decorator(login_required)
	def get(self, request):
		events = BaseEvent.objects.filter(owner=request.user).order_by('-created')

		messages = []

		for event in events:
			factory = EventMessageFactory()
			message = factory.get_event_message(event)
			messages.append(message)

		context = {
			'events' : messages,
		}

		return render(request, 'notification/notification.html', context)