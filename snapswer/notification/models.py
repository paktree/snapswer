from django.db import models
from core.models import TimeStampedModel
from django.contrib.auth.models import User
from core.utils import snapswerresponse
from content.models import Question, Answer
from .utils import EventMessageFactory
from django.dispatch import receiver
from django.db.models.signals import post_save
import core.const

# Create your models here.
class AbstractEvent(TimeStampedModel):
	owner = models.ForeignKey(User)
	acknowledged = models.BooleanField(default=False)


class BaseEvent(AbstractEvent):
	EVENT_TYPE = (
		('ANSWER_ADD', 'ANSWER_ADD'),
	)

	event_type = models.CharField('Type', max_length=30, choices=EVENT_TYPE)
	question = models.ForeignKey(Question, null=True)
	triggers = models.ManyToManyField(User)

	@staticmethod
	def get_latest(user, num=5):
		return BaseEvent.objects.filter(owner=user).order_by('-created')[:num]

	@staticmethod
	def get_unack_count(user):
		return BaseEvent.objects.filter(owner=user, acknowledged=False).count()

	@staticmethod
	@snapswerresponse
	def to_event_response(events, user):
		data = {}
		data["events"] = []
		for event in events:
			event_info = {}
			factory = EventMessageFactory()
			message = factory.get_event_message(event)
			event_info['message'] = message
			event_info['new'] = not event.acknowledged
			event_info['id'] = event.id

			data["events"].append(event_info)

		data["new_count"] = BaseEvent.get_unack_count(user)


		return data, core.const.STATUS_CODE_SUCCESS

	@staticmethod
	@snapswerresponse
	def to_event_read_response():
		data = {}

		return data, core.const.STATUS_CODE_SUCCESS

	@receiver(post_save, sender=Answer)
	def answer_update_handler(sender, instance, created, **kwargs):
		import notification.const 
		if created == True:
			# Check if there such even exist, if it is, update the trigger
			# If it is not, create a new event

			question = instance.question
			event_set = BaseEvent.objects.filter(question=question).filter(acknowledged=False)

			if event_set:
				owner = instance.owner
				event = event_set[0]
				event.triggers.add(owner)
				event.save()
			else:
				event = BaseEvent()
				event.owner = question.owner
				event.question = question
				event.event_type = notification.const.EVENT_ANSWER_ADD
				event.save()
				event.triggers.add(instance.owner)