from .base import *

DEBUG = True

INTERNAL_IPS = ('127.0.0.1',)

INSTALLED_APPS += ('storages',)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'snapswer_db',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'snapswer_su',
        'PASSWORD': 'snapswer**',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

EMAIL_HOST = 'mail.snapswer.com'
EMAIL_HOST_USER = 'mail-noreply@snapswer.com'
EMAIL_HOST_PASSWORD = 'Spicy4**'
EMAIL_USE_TLS = True
EMAIL_PORT =  26

DEFAULT_FILE_STORAGE = 'core.snapswerstorages.MediaFileStorage'

THUMBNAIL_DEFAULT_STORAGE = 'core.snapswerstorages.MediaFileStorage'

STATICFILES_STORAGE = 'core.snapswerstorages.StaticFileStorage'

AWS_ACCESS_KEY_ID = 'AKIAJJ2HBMBPHJXLS2WA'

AWS_SECRET_ACCESS_KEY = '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'

AWS_STORAGE_BUCKET_NAME = 'snapswermedia'

AWS_STORAGE_STATIC_BUCKET_NAME = "snapswerstatic"

AWS_S3_SECURE_URLS = False

STATIC_URL = "http://d2fet7di6yrn4p.cloudfront.net/"

MEDIA_URL = "http://d3jaxnkkghnxz.cloudfront.net/"

COMPRESS_URL = STATIC_URL

COMPRESS_STORAGE = 'core.snapswerstorages.StaticFileStorage'

COMPRESS_ROOT = STATIC_ROOT

COMPRESS_ENABLED = True