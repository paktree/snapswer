from .base import *

DEBUG = True

INTERNAL_IPS = ('127.0.0.1',)

#DATABASES['default'] =  dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'noseatbe_snapswer_db',                      # Or path to database file if using sqlite3.
        'USER': 'noseatbe_wcb',                      # Not used with sqlite3.
        'PASSWORD': 'Spicy4**',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

EMAIL_HOST = 'mail.snapswer.com'
EMAIL_HOST_USER = 'mail-noreply@snapswer.com'
EMAIL_HOST_PASSWORD = 'Spicy4**'
EMAIL_USE_TLS = True
EMAIL_PORT =  26

COMPRESS_ENABLED = True