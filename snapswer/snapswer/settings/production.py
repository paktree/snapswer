from .base import *
import dj_database_url
import os

DEBUG = True

INTERNAL_IPS = ('127.0.0.1',)

INSTALLED_APPS += ('storages',)
INSTALLED_APPS += ('debug_toolbar', 'lockdown')

DATABASES['default'] =  dj_database_url.config()

MIDDLEWARE_CLASSES += (
	'debug_toolbar.middleware.DebugToolbarMiddleware',
    'lockdown.middleware.LockdownMiddleware',
)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
#        'NAME': 'noseatbe_snapswer_db',                      # Or path to database file if using sqlite3.
#        'USER': 'noseatbe_wcb',                      # Not used with sqlite3.
#        'PASSWORD': 'Spicy4**',                  # Not used with sqlite3.
#        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#    }
#}

EMAIL_HOST = 'smtp.sendgrid.net'

if os.getenv('SENDGRID_USERNAME') != None:
	EMAIL_HOST_USER = os.environ['SENDGRID_USERNAME']

if os.getenv('SENDGRID_PASSWORD') != None:
	EMAIL_HOST_PASSWORD = os.environ['SENDGRID_PASSWORD']

EMAIL_USE_TLS = True
EMAIL_PORT = 587

DEFAULT_FILE_STORAGE = 'core.snapswerstorages.MediaFileStorage'

THUMBNAIL_DEFAULT_STORAGE = 'core.snapswerstorages.MediaFileStorage'

STATICFILES_STORAGE = 'core.snapswerstorages.StaticFileStorage'

AWS_ACCESS_KEY_ID = 'AKIAJJ2HBMBPHJXLS2WA'

AWS_SECRET_ACCESS_KEY = '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'

AWS_STORAGE_BUCKET_NAME = 'snapswermedia'

AWS_STORAGE_STATIC_BUCKET_NAME = "snapswerstatic"

AWS_S3_SECURE_URLS = False

STATIC_URL = "http://d2fet7di6yrn4p.cloudfront.net/"

MEDIA_URL = "http://d3jaxnkkghnxz.cloudfront.net/"

COMPRESS_URL = STATIC_URL

COMPRESS_STORAGE = 'core.snapswerstorages.StaticFileStorage'

COMPRESS_ROOT = STATIC_ROOT

COMPRESS_ENABLED = True

LOCKDOWN_PASSWORDS = ('steve', 'lai')