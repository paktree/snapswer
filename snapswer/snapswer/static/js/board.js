
/*
    Follow these steps to use board.js:

    1. Insert the following code to DOM where you want to board to show up
        <div id="board" role="main" style="margin-left: auto;margin-right: auto;">
            <ul id="tiles">
            </ul>
        </div>
    2. Include this js
        <script src="{% static "js/board.js" %}"></script>

    3. Initialize the board (e.g., in document ready)

        var board = new Board(100, '/content/loadsnapswer/{{question.id}}/');

        board.bindScroll();
        board.init();

    Please see /template/content/detail.html for sample code
 */

///////////
// This creates the board instance to specify the board properties
// @param widthPercentage: the width percentage of the board
// @param ajaxUrlArg: the ajax url for fetching addition image data
// @param optionsArg: override default wookmark options with this parameter
function Board (widthPercentage, ajaxUrlArg, scrollEnabledArg, optionsArg){
    Mustache.tags = ['[[', ']]'];

    // default options (see wookmark for addition options: http://www.wookmark.com/jquery-plugin)
    this.options = {
                    autoResize: true, // This will auto-update the layout when the browser window is resized.
                    container: $('#board'), // Optional, used for some extra CSS styling
                    offset: 9, // Optional, the distance between grid items
                    itemWidth: 233 // Optional, the width of a grid item
                };
    
    this.curIndex = 0;

    if (optionsArg) {
        this.options = optionsArg;
    }
    this.ajaxUrl = ajaxUrlArg;

    $('#board').css('width', widthPercentage.toString()+'%'); 

    this.scrollEnabled = true;
    if (scrollEnabledArg != undefined) {
        this.scrollEnabled = scrollEnabledArg;
    }

    var csrftoken = $.cookie('csrftoken');
    $.ajaxSetup({
        headers: {
            'X-CSRFToken': csrftoken
        }
    });

    $("#board").after('<div style="text-align:center;padding-bottom:15px"><div id="spinnerCircle"></div></div>');
}

Board.prototype.addSpinner = function() {
    var opts = {
        lines: 11, // The number of lines to draw
        length: 0, // The length of each line
        width: 4, // The line thickness
        radius: 6, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb
        speed: 1, // Rounds per second
        trail: 65, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    }
    this.spinner = new Spinner(opts).spin($('#spinnerCircle')[0]);
}

Board.prototype.removeSpinner = function() {
    this.spinner.stop();
}

Board.prototype.bindScroll = function() {
    if (this.scrollEnabled)
        $(window).bind('scroll.board', $.proxy(function(e) {this.scrollHnd()}, this));
}

Board.prototype.scrollHnd = function() {
    if ($(window).scrollTop() > $(document).height() - ($(window).height()*2)) {
        $(window).unbind('scroll.board');
        this.loadItems();
    }
}

Board.prototype.applyLayout = function() {
    // Get a reference to your grid items.
    var handler = $('#tiles li');

    // Call the layout function.
    handler.wookmark(this.options);
}

Board.prototype.loadItems = function() {
}

Board.prototype.generateMouseHandler = function(element, id) {
    element.bind('mouseenter', $.proxy(function(e) {this.showButtons(id)}, this));
    element.bind('mouseleave', $.proxy(function(e) {this.hideButtons(id)}, this));
}

Board.prototype.like = function(id) {
}

Board.prototype.follow = function(id) {
}

Board.prototype.showButtons = function(id) {
    $("#pic_tool_bar"+id).fadeIn(80);
    $("#snapswer_mask"+id).css("display", "block");
}

Board.prototype.hideButtons = function(id) {
    $("#pic_tool_bar"+id).fadeOut(80);
    $("#share_popover"+id).popover('hide');
    $("#snapswer_mask"+id).css("display", "none");
}

Board.prototype.init = function() {
    this.loadItems();
}

var ajaxing = false;

// Board for Answers
AnswerBoard.prototype.constructor=AnswerBoard;
function AnswerBoard(widthPercentage, ajaxUrlArg, scrollEnabledArg, optionsArg){
    Board.call(this, widthPercentage, ajaxUrlArg, scrollEnabledArg, optionsArg);
}
AnswerBoard.prototype = Object.create( Board.prototype );

AnswerBoard.prototype.renderBoardDisplay = function(template, templateData) {
    $('#tiles').append(Mustache.render(template, templateData));
    if (templateData.liked) {
        $('#like_label'+templateData.id).html('Unlike');
        $('#answer_like_' + templateData.id).html('<span style="font-size:11.5px" class="snapswer-icon-thumbs-down"></span>');
    
    }
    else
    {
        $('#answer_like_' + templateData.id).html('<span style="font-size:11.5px" class="snapswer-icon-thumbs-up"></span>');
    }

    if (templateData.followed) {
        $('#follow_label'+templateData.id).html('Unfollow');   
    }
    this.generateMouseHandler($("#container"+templateData.id), templateData.id);
}

AnswerBoard.prototype.loadItems = function() {
    if (ajaxing) {
        return false;
    }

    ajaxing = true;

    var urlStr = '';

    if (this.curIndex)
        urlStr = this.ajaxUrl + "?seq_num=" + this.curTimestamp + "&index=" + this.curIndex;
    else
        urlStr = this.ajaxUrl + "?index=0";

    this.addSpinner();
    $.ajax({ type: "GET",
        url: urlStr,
        context: this,
        success: function(data) {
            if (data.status_code == 10004 && !this.curIndex) {
                // empty board
                $.get(board_template, $.proxy(function(templates) {
                    var templateData = {};
                    var template = $(templates).filter('#empty_answer_template').html();

                    $('#board').append(Mustache.render(template, templateData));

                    var self = this;
                    self.applyLayout();

                    this.removeSpinner();

                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        $('#feedback_block').css("display", "none");
                    }

                    ajaxing = false;
                }, this));
                return;
            }

            this.curIndex = data.data.index;
            $.get(board_template, $.proxy(function(templates) {
                for (var i = 0; i < data.data.answers.length; i++) {
                    var answer = data.data.answers[i];
                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        var templateData = 
                        {
                            id: answer.id,
                            owner: answer.owner,
                            owner_id: answer.owner_id,
                            owner_pic: answer.owner_pic,
                            image_url: answer.image,
                            thumb_url: answer.thumb,
                            thumb_width: answer.thumb_width,
                            thumb_height: answer.thumb_height,
                            detail: answer.detail,
                            follow_count: answer.follow_count,
                            like_count: answer.vote_count,
                            liked: answer.vote_by_user,
                            followed: answer.follow_by_user
                        };
                        $('#feedback_block').css("display", "none");
                    } else {
                        var height = parseFloat(answer.thumb_height);
                        var width = parseFloat(answer.thumb_width);
                        var adjusted_height = height*(snapswer_thumb_width/width);
                        var templateData = 
                        {
                            id: answer.id,
                            owner: answer.owner,
                            owner_id: answer.owner_id,
                            owner_pic: answer.owner_pic,
                            image_url: answer.image,
                            thumb_url: answer.thumb,
                            thumb_width: snapswer_thumb_width,
                            thumb_height: adjusted_height,
                            detail: answer.detail,
                            follow_count: answer.follow_count,
                            like_count: answer.vote_count,
                            liked: answer.vote_by_user,
                            followed: answer.follow_by_user
                        };
                    }

                    var template = $(templates).filter('#answer_template').html();
                    this.renderBoardDisplay(template, templateData);

                    $("a#single_image"+templateData.id).fancybox({
                        helpers : {
                            title : {
                                type: 'inside'
                            }
                        }
                    });

                    $(function() {
                        var facebook_url = 'http://www.facebook.com/sharer.php?s=100&p[title]='+question_title+'&p[url]='+window.location+'&p[summary]=Check out this Snapswer!&p[images][0]=http://wvs.topleftpixel.com/photos/stiched_eaton_center_tall_2.jpg';
                        var twitter_url = 'https://www.twitter.com/share?via=snapswer&text=Question of the day: '+question_title+'. Check out this snapswer!&url='+window.location;
                        var google_url = 'https://plus.google.com/share?url=' + window.location;
                        var content = '<div><a href="javascript:void(0)" onClick="openPopup(\''+facebook_url+'\');">Facebook</a></div><div><a href="javascript:void(0)" onClick="openPopup(\''+twitter_url+'\');">Tweet</a></div>';
                        //$('#share_popover'+templateData.id).popover({'placement': 'bottom', 'html': true, 'animation': false, 'content': content});
                        $("#answer_fb_"+templateData.id).click(function() {ga('send', 'event', 'snapswer', 'fb_share', templateData.id); openPopup(facebook_url); });
                        $("#answer_tw_"+templateData.id).click(function() {ga('send', 'event', 'snapswer', 'tw_share', templateData.id); openPopup(twitter_url); });
                        $("#answer_gp_"+templateData.id).click(function() {ga('send', 'event', 'snapswer', 'gp_share', templateData.id); openPopup(google_url); });
                    });
                }
                var self = this;
                self.applyLayout();

                this.removeSpinner();

                ajaxing = false;

                if (data.data.hasMore) {
                    this.bindScroll();
                }
            }, this));
        },
    });
}

AnswerBoard.prototype.like = function(id) {
    $.ajax({ type: "POST",
        url: "/content/vote/answer/"+id+"/",
        context: this,
        success: function(data) {
            if (data.status_code == 10001) {
                $("#like_count_label"+id).html(data.data.count);
                if (data.data.action == "voted") {
                    $("#like_label"+id).html("Unlike");
                    $('#answer_like_' + id).html('<span style="font-size:11.5px" class="snapswer-icon-thumbs-down"></span>');
                    ga('send', 'event', 'snapswer', 'like', id);
                } else {
                    $("#like_label"+id).html("Like");
                    $('#answer_like_' + id).html('<span style="font-size:11.5px" class="snapswer-icon-thumbs-up"></span>');
                    ga('send', 'event', 'snapswer', 'unlike', id);
                }
                
            } else {
                if (data.status_code == 10003) {
                    window.location = "/accounts/login/?next="+window.location.pathname;
                }
            }
        }
    });
}

AnswerBoard.prototype.follow = function(owner_id) {
    $.ajax({ type: "POST",
        url: "/accounts/follow/user/"+owner_id+"/",
        context: this,
        success: function(data) {
            $("#follow_count_label"+owner_id).html(data.data.count);
            if (data.data.action == "follow") {
                $("#follow_label"+id).html("Unfollow");
            } else {
                $("#follow_label"+id).html("Follow");
            }
        }
    });
}

// Board for Questions
QuestionBoard.prototype.constructor=QuestionBoard;
function QuestionBoard(widthPercentage, ajaxUrlArg, scrollEnabledArg, optionsArg){
    Board.call(this, widthPercentage, ajaxUrlArg, scrollEnabledArg, optionsArg);
}
QuestionBoard.prototype = Object.create( Board.prototype );

QuestionBoard.prototype.renderBoardDisplay = function(template, templateData) {
    $('#tiles').append(Mustache.render(template, templateData));

    if (templateData.liked) {
        $('#like_label'+templateData.id).html('Unlike');
        $('#question_like_' + templateData.id).html('<i class="glyphicon glyphicon-thumbs-down"></i>');
    } 
    else
    {
        $('#question_like_' + templateData.id).html('<i class="glyphicon glyphicon-thumbs-up"></i>');
    }

    if (templateData.followed) {
        $('#follow_label'+templateData.id).html('Unfollow');
        $('#question_follow_' + templateData.id).html('<i class="glyphicon glyphicon-eye-close"></i>');
    }
    else
    {   
        $('#question_follow_' + templateData.id).html('<i class="glyphicon glyphicon-eye-open"></i>');
    }

    this.generateMouseHandler($("#container"+templateData.id), templateData.id);
}

QuestionBoard.prototype.loadItems = function() {
    if (ajaxing) {
        return false;
    }

    ajaxing = true;

    var urlStr = '';

    if (this.curIndex)
        urlStr = this.ajaxUrl + "?seq_num=" + this.curTimestamp + "&index=" + this.curIndex;
    else
        urlStr = this.ajaxUrl + "?index=0";

    this.addSpinner();
    $.ajax({ type: "GET",
        url: urlStr,
        context: this,
        success: function(data) {
            this.curIndex = data.data.index;
            $.get(board_template, $.proxy(function(templates) {
                for (var i = 0; i < data.data.summaries.length; i++) {
                    // need to refactor /content/board/
                    var summary = data.data.summaries[i];
                    var templateData = 
                    {
                        id: summary.question.id,
                        thumb_url: summary.answer.thumb_url,
                        thumb_width: summary.answer.thumb_width,
                        thumb_height: summary.answer.thumb_height,
                        title: summary.question.title,
                        follow_count: summary.question.follow_count,
                        like_count: summary.question.vote_count,
                        liked: summary.question.vote_by_user,
                        followed: summary.question.follow_by_user
                    };

                    var template = $(templates).filter('#question_template').html();
                    this.renderBoardDisplay(template, templateData);

                    $(function() {
                        var facebook_url = 'https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocal.snapswer.com%3A8000%2Fcontent%2Fboard%2F'+templateData.id;
                        var twitter_url = 'https://www.twitter.com/share?text=http%3A%2F%2Flocal.snapswer.com%3A8000%2Fcontent%2Fboard%2F'+templateData.id;
                        var content = '<div><a href="javascript:void(0)" onClick="openPopup(\''+facebook_url+'\');">Facebook</a></div><div><a href="javascript:void(0)" onClick="openPopup(\''+twitter_url+'\');">Tweet</a></div>';
                        $('#share_popover'+templateData.id).popover({'placement': 'bottom', 'html': true, 'animation': false, 'content': content});
                    });
                }
                var self = this;
                self.applyLayout();

                this.removeSpinner();

                ajaxing = false;

                if (data.data.hasMore) {
                    this.bindScroll();
                }
            }, this));
        }
    });
}

QuestionBoard.prototype.like = function(id) {
    $.ajax({ type: "POST",
        url: "/content/vote/question/"+id+"/",
        context: this,
        success: function(data, textStatus) {
            $("#like_count_label"+id).html(data.data.count);
            if (data.data.action == "voted") {
                $("#like_label"+id).html("Unlike");
                $('#question_like_' + id).html('<i class="glyphicon glyphicon-thumbs-down"></i>');
            } else {
                $("#like_label"+id).html("Like");
                $('#question_like_' + id).html('<i class="glyphicon glyphicon-thumbs-up"></i>'); 
            }
        }
    });
}

QuestionBoard.prototype.follow = function(id) {
    $.ajax({ type: "POST",
        url: "/content/follow/question/"+id+"/",
        context: this,
        success: function(data) {
            $("#follow_count_label"+id).html(data.data.count);
            if (data.data.action == "follow") {
                $("#follow_label"+id).html("Unfollow");
                $('#question_follow_' + id).html('<i class="glyphicon glyphicon-eye-close"></i>');
    
            } else {
                $("#follow_label"+id).html("Follow");    
                $('#question_follow_' + id).html('<i class="glyphicon glyphicon-eye-open"></i>');
            }
        }
    });
}