var board;
$(document).ready(function() {
    var options = {
        beforeSend: function()
        {
            $('#upload_submit').prop('disabled', true);
            return false;
        },
        success: function(data)
        {
            if (data.status_code == 10002)
            {
                var image_field = valueOrDefault(data.data.error_msg.image);

                if (image_field)
                {
                   var alertArea = document.getElementById("alert-area-upload");
                    alertArea.innerHTML = "<div class=\"alert alert-error\">" + image_field + "</div>";                           
                }
                $('#upload_submit').prop('disabled', false);
            }
            else
            {
                $('#upload_submit').prop('disabled', false);
                location.reload();
            }
        }
    };

    if (is_owner) {
        $("#title_div").attr("contenteditable", "true");
        $("#title_div").keypress(function(e){ 
                    if (e.which == 13) {
                        $("#title_div").blur(); 
                        $.ajax({
                            type: "POST",
                            url: "/content/question/update/"+question_id+"/",
                            data: {title: $("#title_div").text()},
                            success: function(data) {
                                if (data.status_code != 10001) {
                                    alert("Error updating title");
                                }
                            },
                            dataType: "json"
                        });
                    } 
                    return e.which != 13; 
                });

        $("#detail_div").attr("contenteditable", "true");
        $("#detail_div").keypress(function(e){ 
                    if (e.which == 13) {
                        $("#detail_div").blur(); 
                        $.ajax({
                            type: "POST",
                            url: "/content/question/update/"+question_id+"/",
                            data: {detail: $("#detail_div").text()},
                            success: function(data) {
                                if (data.status_code != 10001) {
                                    alert("Error updating detail");
                                }
                            },
                            dataType: "json"
                        });
                    } 
                    return e.which != 13; 
                });
        
    }

    $("#snapswerForm").ajaxForm(options);

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        options = {
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: $('#board'), // Optional, used for some extra CSS styling
            offset: 30, // Optional, the distance between grid items
            itemWidth: mobile_snapswer_thumb_width // Optional, the width of a grid item
        };
        board = new AnswerBoard(100, '/content/loadsnapswer/'+question_id+"/", true, options);
    } else {
        board = new AnswerBoard(100, '/content/loadsnapswer/'+question_id+"/");
    }
    

    $.ajax({ type: "GET",
        url: "/core/getseq/",
        success: function(data) {
            board.curTimestamp = data.seq;
            return false;
        },
        dataType: "json"
    });

    board.init();
});

function sendFeedback() {
    url = "/feedback/";
    $.ajax({
        type: "POST",
        url: url,
        data: {feedback: $("#feedback").val()},
        success: function(data) {
            if (data.status_code == 10001) 
            {
                alert("Thank you for your feedback!");
                parent.$.fancybox.close();
            }
            else 
            {
                alert("Oops...Sorry, there was an erorr, please try again later");
            }
        },
        dataType: "json"
    });
}

function shareWithEmail(snapswer_id) {
    url = "/content/snapswer/emailshare/"+snapswer_id+"/";
    $.ajax({
        type: "POST",
        url: url,
        data: {email: $("#email"+snapswer_id).val()},
        success: function(data) {
            if (data.status_code == 10001) 
            {
                alert("You have shared with your friends!");
                parent.$.fancybox.close();
            }
            else 
            {
                alert("Oops...Sorry, there was an error, please try again later");
            }
        },
        dataType: "json"
    });
}

function valueOrDefault(val, def) {
    if (def == undefined) def = "";
    return val == undefined ? def : val;
}

function validateUrlForm() {
    var $form = $("#snapswerUrlForm");
    var url = $form.attr('action');

    $('#web_upload_submit').prop('disabled', true);

    $.ajax({
        type: "POST",
        url: url,
        data: {image_url: $("#link").val(),
                detail: $("#id_url_detail").val(),
        },
        success: function(data) {
            if (data.status_code == 10002) 
            {
                var image_url_error = valueOrDefault(data.data.error_msg.image_url);

                if (image_url_error)
                {
                    var alertArea = document.getElementById("alert-area-url");
                    alertArea.innerHTML = "<div class=\"alert alert-error\">Cannot find any image from the given URL</div>";
                }

                $('#web_upload_submit').prop('disabled', true);
            }
            else 
            {
                location.reload();
                $('#web_upload_submit').prop('disabled', false);
            }
        },
        dataType: "json"
    });

    return false;
}

function likeQuestion(id) {
    $.ajax({ type: "POST",
        url: "/content/follow/question/"+id+"/",
        context: this,
        success: function(data) {
            if (data.data["action"] == "follow")
            {
                $("#id_follow_question").html("Unfollow this question");
            }
            else
            {
                $("#id_follow_question").html("Follow this question");
            }
        }
    });
}

function likeCategory(id) {
    $.ajax({ type: "POST",
        url: "/content/follow/category/"+id+"/",
        context: this,
        success: function(data) {
            if (data.data["action"] == "follow")
            {
                $("#id_follow_category").html("Unfollow");
            }
            else
            {
                $("#id_follow_category").html("Follow");
            }
        }
    });
}

function createUploadAnswerForm() {
    if (!is_logged_in) {
        window.location = "/accounts/login/?next="+window.location.pathname;
        return;
    }
    var form = '<div class="modal-header"><h4 id="myModalLabel"><strong>Add a Snapswer from your computer</strong></h4></div><div class="modal-body"><form role="form" id="snapswerForm" action="'+upload_snapswer_url+'?next='+window.location.pathname+'" method="post" enctype="multipart/form-data" onsubmit="return uploadImgForm();">';
    form += csrf_token;
    form += '<div class="form-group" id="alert-area"></div><div class="form-group"><input type="file" name="image" id="imgInp" class="image-input-text-box"></div><div class="form-group"><div id="previewUploadImage"></div></div><div class="form-group"><input class="form-control input-lg" cols="40" id="id_detail" name="detail" placeholder="Add a single line of descriptions!" rows="3"></input></div><button id="upload_submit" type="submit" class="btn btn-primary btn-lg" disabled>Submit</button></form></div>';
    $("#uploadAnswerModal").html(form);

    $("#imgInp").change(function(){
        readURL(this);
    });
}

function createUploadUrlForm() {
    if (!is_logged_in) {
        window.location = "/accounts/login/?next="+window.location.pathname;
        return;
    }
    var form = '<div class="modal-header"><h4 id="myModalLabel"><strong>Add a Snapswer from a website</strong></h4></div><div class="modal-body image-link-input-text-box"><form id="snapswerUrlForm" action="'+upload_snapswer_url+'" method="post" enctype="multipart/form-data" onsubmit="return validateUrlForm();">';
    form += csrf_token;
    form += '<div class="form-group" id="alert-area-url"></div><div class="form-group"><input type="text" id="link" name="image_url" class="form-control input-lg" placeholder="http://"></div><div class="form-group" id="preview_url"><div id="spinner"></div></div><div class="form-group"><input class="form-control input-lg" cols="40" id="id_url_detail" name="detail" placeholder="Add a single line of descriptions!" rows="3"></input></div><input id="web_upload_submit" class="btn btn-primary btn-lg" type="submit" value="Submit" disabled></form></div></div>';
    $("#addAnswerWebModal").html(form);

    $('#link').on('input', function(){
        $('#spinner').html("<img src='"+spinner_gif+"'></img>");

        if (stoppedTyping) clearTimeout(stoppedTyping);
        stoppedTyping = setTimeout(function(){
            preview();
        }, 2000);
    });
}

var stoppedTyping;
var spinner;


// image url preview
function preview() {
    var image = new Image();
    image.src = $('input[id=link]').val();

    image.onload = function() {   
        if (this.width < 300 || this.height < 300) {
            alert("Please upload picture that has minimum size of 300x300.");
        } else {
            $('#web_upload_submit').prop('disabled', false);
            $('div[id=preview_url]').hide().html('<img class="image-preview" onerror="/*alert(\'The image could not be loaded.\')*/" src="'+$('input[id=link]').val()+'">').fadeIn('slow');
        }

        $('#spinner').html("");
    }
}

// file preview
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result;
            image.onload = function() {
                if (this.width < 300 || this.height < 300) {
                    alert("Please upload picture that has minimum size of 300x300.");
                } else {
                    $('#upload_submit').prop('disabled', false);
                    $('#previewUploadImage').attr('src', e.target.result);
                    $('div[id=previewUploadImage]').hide().html('<img class="image-preview" src="'+e.target.result+'"><br>').fadeIn('slow');
                }
            };
        }

        reader.readAsDataURL(input.files[0]);
    }
}