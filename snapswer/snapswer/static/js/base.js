var mobile_snapswer_thumb_width = 300.0;
var snapswer_thumb_width = 230.0;

function notified() {
    $.ajax({ type: "GET",
        url: "/notification/acknowledge/",
        success: function(data) {
            $("#notification-badge").html("0");
        }
    });
}
$(document).ready(function() {
    /* This is basic - uses default settings */
    
    /* Using custom settings */
    $("a#inline").fancybox({
        'hideOnContentClick': true,
        "autoSize" : false,
        "width"    : "auto",
        "height"   : "auto",
        "maxWidth" : 800
    });

    $("a#feedback_inline").fancybox({
        'hideOnContentClick': true,
        "autoSize" : false,
        "width"    : "auto",
        "height"   : "auto",
        "maxWidth" : 800
    });

    $("a#addSnapswer").fancybox({
        'hideOnContentClick': true,
        "autoSize" : true,
        "width"    : "auto",
        "height"   : "auto",
        "maxWidth" : 800,
        "fitToView": true
    });

    $("#iframeFancyBox").fancybox({
        'width'             : '400px',
        'height'            : '75%',
        'autoScale'         : false,
        'autoDimensions'    : false,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'type'              : 'iframe'
    });

    $("a#prize_inline").fancybox({
        'hideOnContentClick': true,
        "autoSize" : false,
        "width"    : "auto",
        "height"   : "auto",
        "maxWidth" : 800
    });    

    $(window).bind('scroll.global', function(){
        if ($(window).scrollTop() > 500) {
            $('#backToTop').fadeIn(1000);
        } else {
            $('#backToTop').fadeOut(1000);
        }
    });

    $("#backToTop").bind('click', function() {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    })

});

function openPopup(url) {
    window.open(url, "popup_id", "scrollbars,resizable,width=500,height=350");
}
function handleLogin() {
    window.location = "/accounts/login/?next="+window.location.pathname;
}
function handleLogout() {
    window.location = "/accounts/logout/?next="+window.location.pathname;
}