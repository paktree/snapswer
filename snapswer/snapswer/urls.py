from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from core.views import HomeView, FeedbackView, AboutUsView, PrivacyView, TermsConditionsView, TermsUserView, TermsServiceView, ContactUsView, AcknowledgementView
from content.views import LoadQuestionView, QuestionOfDay
from django.conf import settings
from accounts.views import PublicProfile

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'snapswer.views.home', name='home'),
    # url(r'^snapswer/', include('snapswer.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts_api/', include('accounts.api')),
    url(r'^accounts/', include('accounts.urls', namespace="accounts")),
    url(r'^content/', include('content.urls', namespace="content")),
    url(r'^$', QuestionOfDay.as_view(), name="home"),
    url(r'^home/$', QuestionOfDay.as_view()),
    #url(r'^latest/$', HomeView.as_view()),
    #url(r'^popular/$', HomeView.as_view()),
    #url(r'^follow/$', HomeView.as_view()),
    url(r'^category/(?P<category>\w+)/$', HomeView.as_view()),
    url(r'^core/', include('core.urls', namespace="core")),
    url(r'^notification/', include('notification.urls', namespace="notification")),

    url(r'^(?P<username>\w+).(?P<id>\d+)/$', PublicProfile.as_view(), name="public_profile"),
    # ugly solution for built in password retrievial...but it works
    url(r'^account/', include('django.contrib.auth.urls')),
    url(r'^feedback/', FeedbackView.as_view()),
    (r'^facebook/', include('django_facebook.urls')),
    (r'^accounts/', include('django_facebook.auth_urls')), #Don't add this line if you use django registration or userena for registration and auth.
    url(r'^about/', AboutUsView.as_view()),
    url(r'^terms/', PrivacyView.as_view()),
    url(r'^terms_and_conditions/', TermsConditionsView.as_view()),
    url(r'^terms_of_user/', TermsUserView.as_view()),
    url(r'^terms_of_service/', TermsServiceView.as_view()),
    url(r'^contact/', ContactUsView.as_view()),
    url(r'^acknowledgement/', AcknowledgementView.as_view()),
)

urlpatterns += patterns('',(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))

urlpatterns += staticfiles_urlpatterns()
urlpatterns += patterns('',(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATICFILES_DIRS[0]}))
