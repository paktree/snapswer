from storages.backends.s3boto import S3BotoStorage
from django.conf import settings
import urlparse

def domain(url):
    return urlparse.urlparse(url).hostname

class MediaFileStorage(S3BotoStorage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket'] = settings.AWS_STORAGE_BUCKET_NAME
        kwargs['custom_domain'] = domain(settings.MEDIA_URL)
        super(MediaFileStorage, self).__init__(*args, **kwargs)

class StaticFileStorage(S3BotoStorage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket'] = settings.AWS_STORAGE_STATIC_BUCKET_NAME
        kwargs['custom_domain'] = domain(settings.STATIC_URL)
        super(StaticFileStorage, self).__init__(*args, **kwargs)