from django.db import models
from django.contrib.auth.models import User
from .managers import LatestManager
from .utils import snapswerresponse
import core.const
from datetime import datetime

class TimeStampedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    modified = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    objects = models.Manager()

    latest = LatestManager()

    @staticmethod
    def get_by_seq_index(qs, index, count, seq=None):
        if seq:
            time_ref = datetime.fromtimestamp(int(seq))
            query_set = qs.order_by('-created').filter(created__lte=time_ref)
        else:
            query_set = qs.all().order_by('-created')

        range_end = 0

        total_cnt = query_set.count()

        if total_cnt < (index + count):
            range_end = total_cnt
        else:
            range_end = index + count

        query_set = query_set[index:range_end]

        more = True
        
        if total_cnt == range_end:
            more = False

        return query_set, more
                
    class Meta:
        abstract = True

class VoteableModel(models.Model):
    votes = models.ManyToManyField(User, related_name="%(app_label)s_%(class)s_votes")

    def get_vote_count(self):
        return self.votes.count()

    def remove_vote(self, user):
        self.votes.remove(user)

    def add_vote(self, user):
        self.votes.add(user)

    @staticmethod
    @snapswerresponse
    def to_vote_response(vote, add):
        action = ""
        if add:
            action = "voted"
        else:
            action = "unvoted"

        vote_info = { "action" : action, 
                      "count" : vote.get_vote_count() 
                    }
        
        return vote_info, core.const.STATUS_CODE_SUCCESS

    class Meta:
        abstract = True

class FollowableModel(models.Model):
    followers = models.ManyToManyField(User, related_name="%(app_label)s_%(class)s_followers")

    def get_follow_count(self):
        return self.followers.count()

    def follow(self, user):
        self.followers.add(user)

    def unfollow(self, user):
        self.followers.remove(user)

    def is_followed(self, user):
        try :
            self.followers.get(id=user.id)
            return True
        except User.DoesNotExist:
            return False

    @staticmethod
    @snapswerresponse
    def to_follow_response(follow, add):
        action = ""
        if add:
            action = "follow"
        else:
            action = "unfollow"

        follow_info = { "action" : action ,
                        "count" : follow.get_follow_count()
                      }

        return follow_info, core.const.STATUS_CODE_SUCCESS

    class Meta:
        abstract = True

class FeedbackModel(TimeStampedModel):
    feedback = models.TextField(max_length=1000)

