from django.forms import ModelForm
from .models import FeedbackModel

class FeedbackForm(ModelForm):
	class Meta:
		model = FeedbackModel

	def save(self, commit=True):
		feedback = super(FeedbackForm, self).save(commit=False)

		if commit:
			feedback.save()

		return feedback