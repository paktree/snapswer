from django.db import models
from datetime import datetime, date, timedelta
from django.utils.timezone import now as utcnow

class LatestManager(models.Manager):
	def get_query_set(self):
		return super(LatestManager, self).get_query_set().order_by("-created")
