from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import os

class Command(BaseCommand):

	def handle(self, *args, **options):
		filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "mockdata/username.txt")
		f = open(filename)
		password = "snapswergogogo"

		for line in f:
			name = line.strip()
			print "generating " + name
			email = name + "_snapswer1234@gmail.com"

			user = User.objects.create_user(name, email, password)
			user.save()