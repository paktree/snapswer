from django.core.management.base import BaseCommand, CommandError
from content.utils import ContentCacheManager
from django.core.cache import cache

class Command(BaseCommand):
	def handle(self, *args, **options):
		print "Clearing cache..."
		cache.clear()
		# invoke a list of static methods that require cache
		print "Updating cache..."
		ContentCacheManager.update_cache()

