from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from content.models import Question, Answer, Category
from content.forms import AnswerForm
import random
from django.core.files.uploadedfile import SimpleUploadedFile
import os

class Command(BaseCommand):
	def handle(self, *args, **options):
		user_list = []

		print "Generating Users..."
		# create 50 user
		for i in range(5):
			user = User(username="tester" + str(i), password="1234")
			user.save()
			user_list.append(user)

		random_list = []
		random_list.append("Who is the best actor in 1900's?")
		random_list.append("What is the best moment in 2012 Olympic Game?")
		random_list.append("Who is the best president in United States?")
		random_list.append("What kind of motocycle you have at home?")
		random_list.append("Which historical war is the most unforgetable?")
		random_list.append("What kind of dog has the highest IQ?")
		random_list.append("Which fast food is the unhealtiest?")
		random_list.append("Where is the best place to visit in Los Angeles?")
		random_list.append("What is the most spiciest dish?")
		random_list.append("What is the best car in 2013?")
		random_list.append("Which is the best movie in 2000's?")
		random_list.append("What is the best resturant in San Fransico?")
		random_list.append("What is the scariest place in United State?")
		random_list.append("Which kung fu movie inspire you the most?")
		random_list.append("What is your current most expensive handbag?")
		random_list.append("What is the most intesting video game in your childhood?")
		random_list.append("What is your favorite milk shake?")
		random_list.append("What is your most favorite fast food?")
		random_list.append("What is your best photo that you have ever taken")
		random_list.append("What is the best travel city in France?")
		random_list.append("What is the most unforgetable moment in your life?")
		random_list.append("What is the best childhood snack")
		random_list.append("What is the best chocolate that you have ever had?")
		
		random_des = []
		random_des.append("This is a short description")

		print "Generating Questions..."
		# create 20 random questions for each user
		question_list = []
		count = 0
		for user in user_list:
			for i in range(3):
				question = Question()
				question.title = str(count) + " " + random_list[random.randrange(0,22,1)]
				question.detail = "This is a short description"
				question.owner = user
				question.category = Category.objects.get(id=1)
				question.save()
				question_list.append(question)
				count = count + 1

				# random 30 time to add votes
				for i in range(5):
					user_id = random.randrange(1,5,1)
					question.votes.add(User.objects.get(id=user_id))

		print "Generating Answer..."
		path = os.path.abspath(os.path.dirname(__file__))
		random_pic = []
		random_pic.append(os.path.join(path, "mockdata/pic1.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic2.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic3.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic4.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic5.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic6.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic7.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic8.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic9.jpg"))
		random_pic.append(os.path.join(path, "mockdata/pic10.jpg"))

		answer_list = []
		for question in question_list:
			for i in range(3):
				data = {"detail": "This is an answer detail"}
				upload_file = open(random_pic[random.randrange(0,9,1)], 'rb')
				file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
		
				form = AnswerForm(data, file_dic)
				answer = form.save(user=user_list[random.randrange(0,4,1)],
								   question=question)
				answer_list.append(answer)

				# random 30 time to add votes
				for i in range(6):
					user_id = random.randrange(1,5,1)
					answer.votes.add(User.objects.get(id=user_id))
