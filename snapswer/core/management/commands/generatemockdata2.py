from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from content.models import Question, Answer, Category
from content.forms import AnswerForm
import random
from django.core.files.uploadedfile import SimpleUploadedFile
import os
from django.conf import settings
from django.core.management import call_command

class Command(BaseCommand):
	num_user = 10
	num_question = 10
	num_answer = 5
	num_question_vote = 30
	num_answer_vote = 20
	category_range = 27

	mock_question_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "mockdata/mockquestions.txt")
	user_list = []
	question_list = []
	answer_list =[]

	def handle(self, *args, **options):

		call_command('loaddata', 'content/fixtures/category.xml')

		self.generate_user()

		for user in self.user_list:
		 	self.generate_question(user)

		for question in self.question_list:
		 	self.generate_answer(question)

	def generate_user(self):
		print "Generating users..."
		for i in range(self.num_user):
			username = "mockuser" + str(i)
			print username
			user = User.objects.create_user(username, "tester@snapswer.com", "1234")
			user.save()
			user.profile.about = "This sort of reference can be useful when resolving circular import dependencies between two applications. A database index is automatically created on the ForeignKey. You can disable this by setting db_index to False. You may want to avoid the overhead of an index if you are creating a foreign key for consistency rather than joins, or if you will be creating an alternative index like a partial or multiple column index.Database Representation Behind the scenes, Django"
			user.profile.save()
			self.user_list.append(user)


	def generate_question(self, user):
		print "Generating questions..."

		question_titles = []
		
		with open(self.mock_question_file, 'r') as f:
			for line in f:
				question_titles.append(line)

		for i in range(self.num_question):
			print "User " + str(user.id) + " Question " + str(i)
			question = Question()
			question_title = question_titles[random.randrange(0, len(question_titles) - 1, 1)]

			question.title = str(i) + " " + question_title
			question.detail = "I don't mean to sound all fiscally conservative but as interesting as these things are, I just feel some the vast resources could be better used."
			question.owner = user
			category_id = random.randrange(1, self.category_range + 1, 1)
			question.category = Category.objects.get(id=category_id)
			question.save()
			self.question_list.append(question)

			random_vote = random.randrange(0, self.num_question_vote, 1)
			for j in range(random_vote):
				question.votes.add(self.user_list[random.randrange(0, len(self.user_list), 1)])

	def generate_answer(self, question):
		print "Generating answers..."
		image_list = []

		print "Get all random images..."
		folder_list = ["animal", "architecture", "fashion", "food", "history", "nature", "photography", "sport", "travel"]

		for folder in folder_list:
			folder_path = ''.join(["snapswer/media/mockfiles/", folder])
			path = os.path.join(os.path.abspath(settings.PROJECT_ROOT), folder_path)
			files = os.listdir(path)
	
			for f in files:
				full_path = os.path.join("mockfiles/" + folder + "/", f)
				image_list.append(full_path)

		for i in range(self.num_answer):
			print "Question " + str(question.id) + " Answer " + str(i)
			answer = Answer()
			answer.image = image_list[random.randrange(0, len(image_list) - 1, 1)]
			answer.question = question
			answer.owner = self.user_list[random.randrange(0, len(self.user_list), 1)]
			answer.save()
			self.answer_list.append(answer)

			random_vote = random.randrange(0, self.num_answer_vote, 1)
			for j in range(random_vote):
				question.votes.add(self.user_list[random.randrange(0, len(self.user_list), 1)])
