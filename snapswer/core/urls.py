from django.conf.urls import patterns, url
from .views import HomeView, GetSeqView

urlpatterns = patterns('',
	# API View
	url(r'getseq/$', GetSeqView.as_view(), name="get_seq")
)