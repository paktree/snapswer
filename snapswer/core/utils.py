import re
import json
import datetime
from time import mktime
from django.utils import simplejson
import json
import core.const
import os
import random
import glob
from django.conf import settings
from os.path import basename

def json_string_to_list(content):
	string = content
	string = string[1:-1]
	string = re.sub('[\\\]', '', string)
	ret = simplejson.loads(string)
	return ret

class SnapswerJsonEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, datetime.datetime):
			return int(mktime(obj.timetuple()))

		return json.JSONEncoder.default(self, obj)

class snapswerresponse(object):
	func = None
	def __init__(self, func):
		self.func = func

	def __call__(self, *args, **kargs):
		response = {}
		data, status_code = self.func(*args, **kargs)
		response["data"] = data
		response["status_code"] = status_code
		response = json.dumps(response, cls=SnapswerJsonEncoder)

		return response

@snapswerresponse
def to_unsuccess_empty_response():
	data = {}
	return data, core.const.STATUS_CODE_UNSUCCESS

@snapswerresponse
def to_success_empty_response():
	data = {}
	return data, core.const.STATUS_CODE_SUCCESS

def get_background_image():
	path = settings.STATICFILES_DIRS[0] + "/images/background/"

	bg_images = glob.glob(path + "*.jpg")
	idx = random.randrange(0, len(bg_images))
	bg_image = bg_images[idx]
	bg_image = basename(bg_image)

	bg_image = os.path.join(settings.STATIC_URL + "/images/background/", bg_image)
	return bg_image	

@snapswerresponse
def to_require_login_response():
	data = {}
	return data, core.const.STATUS_CODE_REQUIRE_LOGIN