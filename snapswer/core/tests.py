"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client
from django.utils import simplejson
from StringIO import StringIO

from content.forms import QuestionForm
from content.models import Category
from .forms import FeedbackForm
from django.contrib.auth.models import User 
from .models import FeedbackModel

class SnapswerTestCase(TestCase):
    fixtures = ['test_fixture']
    
class GetSeqViewTest(SnapswerTestCase):
    def test_get_seq(self):
        client = Client()
        resp = client.get('/core/getseq/')
        io = StringIO(resp.content)
        ret = simplejson.load(io)
        
        self.assertEqual(ret['seq'] != 0, True)

    def test_temp(self):
        category = Category()
        category.name = "temp"
        category.save()
        data = {"title": "1234", "detail": "hi", "category": "1"}
        form = QuestionForm(data=data)

class FeedbackFormTest(TestCase):
    def test_basic(self):
        data = {"feedback": "This is a feedback"}
        form = FeedbackForm(data)

        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)

class FeedbackViewTest(TestCase):
    def test_view(self):
        u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        u1.save()

        client = Client()
        ret = client.login(username="tester", password="12345")

        self.assertEqual(ret, True)

        data = {"feedback": "This is a feedback"}

        client.post('/feedback/', data=data, Follow=True)

        self.assertEqual(FeedbackModel.objects.count(), 1)

        data = {"feedback": "This is a feedback"}

        client.post('/feedback/', data=data, Follow=True)

        self.assertEqual(FeedbackModel.objects.count(), 2)

