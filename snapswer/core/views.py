# Create your views here.
from django.views.generic import View
from django.shortcuts import render
from django.utils import simplejson
from datetime import datetime
import time
from django.http import HttpResponse
import json
from content.models import Question, Category
from .utils import SnapswerJsonEncoder
from django.contrib.auth.models import User
from .models import FollowableModel
from .utils import to_unsuccess_empty_response, to_success_empty_response

from django.http import Http404

from content.forms import QuestionForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .forms import FeedbackForm

class HomeView(View):
	def get(self, request, category=None):
		form = QuestionForm()
		context = { 'temp' : form }
		return render(request, 'content/content.html', context) 
		
class GetSeqView(View):
	def get(self, request):
		timestamp = time.mktime(datetime.now().timetuple())
		time_ref = datetime.fromtimestamp(int(timestamp))
		response_data = {}
		response_data['seq'] = int(timestamp)
		data = simplejson.dumps(response_data)
		return HttpResponse(data, mimetype="json")		

class FollowView(View):
	follow_type = None

	def post(self, request, id):
		obj = None

		if self.follow_type == 'question':
			obj = Question.objects.filter(id=id)
		elif self.follow_type == 'user':
			user = User.objects.filter(id=id)

			if len(user) == 1:
				user = user[0]
				obj = [user.profile]

				# can't follow itself
				if user == request.user:
					obj = None
		elif self.follow_type == 'category':
			obj = Category.objects.filter(id=id)
		else:
			raise Http404

		data = {}

		if obj == None:
			data["status"] = 0
			data = json.dumps(data, cls=SnapswerJsonEncoder)
			return HttpResponse(data, mimetype="json")

		if len(obj) == 1:
			obj = obj[0]

			user = obj.followers.filter(username=request.user.username)

			if user:
				# unfollow
				obj.unfollow(request.user)
				data = FollowableModel.to_follow_response(obj, False)
			else:
				obj.follow(request.user)
				data = FollowableModel.to_follow_response(obj, True)

		else:
			data = to_unsuccess_empty_response()

		return HttpResponse(data, mimetype="json")

class FeedbackView(View):
	@method_decorator(login_required)
	def post(self, request):
		form = FeedbackForm(request.POST)
		form.save()

		data = to_success_empty_response()

		return HttpResponse(data, mimetype="json")

class AboutUsView(View):
	def get(self, request):
		return render(request, 'core/about.html')

class DeleteView(View):
	content_type = None
	
	@method_decorator(login_required)
	def post(self, request, id):
		val = False
		if content_type == "snapswer":
			val = Answer.delete_snapswer(id, request.user)
		elif content_type == "question":
			val = Question.delete_question(id, request.user)	

		if not val:
			data = to_unsuccess_empty_response()
		else:
			data = to_success_empty_response()

		return HttpResponse(data)

class PrivacyView(View):
	def get(self, request):
		return render(request,  'core/terms_privacy.html')

class TermsConditionsView(View):
	def get(self, request):
		return render(request,  'core/terms_conditions.html')

class TermsUserView(View):
	def get(self, request):
		return render(request,  'core/terms_use.html')

class TermsServiceView(View):
	def get(self, request):
		return render(request,  'core/terms_service.html')

class ContactUsView(View):
	def get(self, request):
		return render(request,  'core/contact_us.html')

class AcknowledgementView(View):
	def get(self, request):
		return render(request,  'core/acknowledgement.html')