from django.conf.urls import patterns, url
from .views import LoginAPIView, RegisterAPIView

urlpatterns = patterns('',
	url(r'^login/$', LoginAPIView.as_view(), name="login"),
	url(r'^register/$', RegisterAPIView.as_view(), name="register"),
)