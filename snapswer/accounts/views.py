from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate, login, logout
from django.core import serializers
from rest_framework.authtoken.models import Token
from django.views.generic.base import View
from django.shortcuts import render_to_response, render, redirect
from django.utils.http import is_safe_url
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm
from .forms import ProfileForm, SnapswerUserCreationForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from content.models import Answer, Question
from django.http import HttpResponse, HttpResponseBadRequest
import content.const
from django.http import Http404
from .const import Constant
from core.utils import to_success_empty_response, get_background_image
from .forms import SnapswerAuthenticaionForm
import os
import glob
import random

class LoginView(View):
	def post(self, request):
		# TODO FIX ME
		redirect_to = request.REQUEST.get('next', '')

		form = SnapswerAuthenticaionForm(data=request.POST)
		if form.is_valid():
			if not is_safe_url(url=redirect_to, host=request.get_host()):
				redirect_to = settings.LOGIN_REDIRECT_URL

			login(request, form.get_user())
			return redirect(redirect_to)
		else:
			form = SnapswerAuthenticaionForm()
			context = {
				'error': True,
				'form': form,
				'next': request.REQUEST.get('next', '')
			}

			return render(request, 'accounts/login.html', context)

	def get(self, request):
		form = SnapswerAuthenticaionForm()
		
		context = {
	        'error': False,
	        'form': form,
	        'next': request.REQUEST.get('next', '')
	    }
		
		return render(request, 'accounts/login.html', context)

class RegisterView(View):
	def post(self, request):
		redirect_to = request.REQUEST.get('next', '')

		form = SnapswerUserCreationForm(request.POST)
		if form.is_valid():
			username = form.clean_username()
			password = form.clean_password2()
			form.save()
			user = authenticate(username=username, password=password)
			login(request, user)

			return redirect("/home/")
		else:
			context = {'form': form,
						'error': False,
						'next': request.REQUEST.get('next', '') }
			return render(request, 'accounts/register.html', context)

	def get(self, request):
		form = SnapswerUserCreationForm()
		context = {'form': form, 
					'error': True }
		return render(request, 'accounts/register.html', context)

class LoginOutView(View):
	def get(self, request):
		redirect_to = request.REQUEST.get('next', '')

		logout(request)
		return redirect(redirect_to)

class ProfileView(View):
	@method_decorator(login_required)
	def get(self, request):
		form = ProfileForm(instance=request.user.profile, 
						   initial={'last_name': request.user.last_name, 
						   			'first_name': request.user.first_name,
						   			'email': request.user.email,
							})
		context = {'form': form,}

		return render(request, 'accounts/profile.html', context)

	@method_decorator(login_required)
	def post(self, request):
		form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)

		if not form.is_valid():
			pass
		else:
			form.save()
			form = ProfileForm(instance=request.user.profile,
							   initial={'last_name': request.user.last_name, 
						   			'first_name': request.user.first_name,
						   			'email': request.user.email,
								})

		context = {'form': form,}

		return render(request, 'accounts/profile.html', context)

class PasswordChangeView(View):
	@method_decorator(login_required)
	def get(self, request):
		form = PasswordChangeForm(user=request.user)
		context = {'form': form,}

		return render(request, 'accounts/password.html', context)

	@method_decorator(login_required)
	def post(self, request):
		form = PasswordChangeForm(request.user, data=request.POST)

		if not form.is_valid():
			pass
		else:
			form.save()

		context = {'form': form,}

		return render(request, 'accounts/password.html', context)

# API
class LoginAPIView(APIView):
	def post(self, request, format=None):
		email = request.DATA['email']
		password = request.DATA['password']

		user_cache = authenticate(email=email,
								 password=password)

		if user_cache is None:
			data = serializers.serialize('json', [])
			return Response(data, status=status.HTTP_401_UNAUTHORIZED)
		elif not user_cache.is_active:
			return Response(data, status=status.HTTP_401_UNAUTHORIZED)

		token, created = Token.objects.get_or_create(user=user_cache)

		data = serializers.serialize('json', [token])
		return Response(data, status=status.HTTP_201_CREATED)

class RegisterAPIView(APIView):
	def post(self, request, format=None):
		username = request.DATA['username']
		password = request.DATA['password']
		
		num = User.objects.filter(username=username).count()
		
		if num != 0:
			data = serializers.serialize('json', [])
			return Response(data, status=status.HTTP_401_UNAUTHORIZED)
		
		user = User()
		user.username = username
		user.set_password(password)
		user.save()

		user = authenticate(username=username, password=password)

		if not user:
			data = serializers.serialize('json', [])
			return Response(data, status=status.HTTP_401_UNAUTHORIZED)

		token, created = Token.objects.get_or_create(user=user)
		
		data = serializers.serialize('json', [token], use_natural_keys=True)

		return Response(data, status=status.HTTP_201_CREATED)

class UserAnswerView(View):
	def get(self, request, username):

		if not 'index' in request.GET:
			return HttpResponseBadRequest(mimetype='json')

		index = request.GET['index']
		index = int(index)

		user = User.objects.filter(username=username)

		if user.count() == 1:
			user = user[0]
			
			answers = user.answer_set.all().order_by("created")
			
			if 'seq_num' in request.GET:
				timestamp = request.GET['seq_num']
				answer_objs, more = Answer.get_by_seq_index(answers, index, content.const.SNAPSWER_PER_LOAD, timestamp)

			else:
				answer_objs, more = Answer.get_by_seq_index(answers, index, content.const.SNAPSWER_PER_LOAD)

			new_index = index + answer_objs.count()
				
			data = Answer.to_detail_answer_response(answers, new_index, request.user, more)
		
			return HttpResponse(data, mimetype="json")
		else:
			return HttpResponseBadRequest(mimetype="json")

class PublicProfile(View):
	def get(self, request, username, id):
		context = {"username" : username}

		try:
			current_user = User.objects.get(id=id, username=username)
			
			followed = False

			try:
				current_user.profile.followers.get(id=request.user.id)
				followed = True
			except User.DoesNotExist:
				followed = False

			question_cnt = Question.objects.filter(owner=current_user).count()
			
			snapswer_cnt = Answer.objects.filter(owner=current_user).count()

			vote_question_cnt = 0

			temp = Question.get_vote_question(current_user)

			if temp:
				vote_question_cnt = temp.count()

			follow_question_cnt = 0

			temp = Question.get_follow_questions(current_user)

			if temp:
				follow_question_cnt = temp.count()

			context = { "username" : current_user.username,
						"id": current_user.id,
						"followed": followed,
						"question_cnt": question_cnt,
						"snapswer_cnt": snapswer_cnt,
						"vote_question_cnt": vote_question_cnt,
						"follow_question_cnt": follow_question_cnt,
						"profile": current_user.profile,
					  }

		except User.DoesNotExist:
			raise Http404

		return render(request, 'accounts/public_profile.html', context)
		

class PublicProfileBoard(View):
	def get(self, request, username, board_type):
		if not 'index' in request.GET:
			return HttpResponseBadRequest(mimetype='json')

		index = request.GET['index']
		index = int(index)

		timestamp = None
		if ('seq_num' in request.GET):
			timestamp = request.GET['seq_num']

		if board_type == Constant.BOARD_TYPE_SNAPSWER:
			user = User.objects.filter(username=username).prefetch_related("answer_set")
			
			if user.count() != 1:
				raise Http404

			user = user[0]

			answers = user.answer_set.all().order_by("created")
			answer_objs, more = Answer.get_by_seq_index(answers, index, content.const.SNAPSWER_PER_LOAD, timestamp)

			new_index = index + answer_objs.count()

			data = Answer.to_detail_answer_response(answers, new_index, request.user, more)

			return HttpResponse(data, mimetype="json")
	
		elif board_type == Constant.BOARD_TYPE_QUESTION:
			user  = User.objects.filter(username=username).prefetch_related("question_set")
			if user.count() != 1:
				raise Http404

			user = user[0]

			questions = user.question_set.all().order_by("created")

			question_objs, more = Question.get_by_seq_index(questions, index, content.const.QUESTION_PER_LOAD, timestamp)

			new_index = index + question_objs.count()
			data = Question.to_summarize_question(question_objs, user, index, more)

			return HttpResponse(data, mimetype="json")
		elif board_type == Constant.BOARD_TYPE_FOLLOWING:
			user = User.objects.filter(username=username)

			if user.count() != 1:
				raise Http404

			user = user[0]
			questions = Question.get_follow_questions(user)

			if questions:
				questions, more = Question.get_by_seq_index(questions, index, content.const.QUESTION_PER_LOAD, timestamp)

				new_index = questions.count() + index

				data = Question.to_summarize_question(questions, user, index, more)
		
				return HttpResponse(data, mimetype="json")

			else:
				data = to_success_empty_response()
				return HttpResponse(data, mimetype="json")

		elif board_type == Constant.BOARD_TYPE_VOTED:
			user = User.objects.filter(username=username)

			if user.count() != 1:
				raise Http404

			user = user[0]

			questions = Question.get_vote_question(user)


			if questions:
				question_objs, more = Question.get_by_seq_index(questions, index, content.const.QUESTION_PER_LOAD, timestamp)
				new_index = question_objs.count() + index

				data = Question.to_summarize_question(question_objs, user, index, more)
				return HttpResponse(data, mimetype="json")
			else:

				data = to_success_empty_response()
				return HttpResponse(data, mimetype="json")
		else:
			raise Http404



