"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client
from django.contrib.auth import authenticate
from django.core import serializers
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .forms import SnapswerUserCreationForm
from django.utils import simplejson
from django.contrib.auth.models import User
from .models import Profile
from .const import Constant
from .forms import ProfileForm
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test.utils import override_settings
from django.test.client import Client
import json
import os
import re
from .views import UserAnswerView
from content.models import Question, Answer, Category
from django.test.client import Client
from content.forms import AnswerForm
from core.tests import SnapswerTestCase
from django.core.cache import cache

class AppUserTest(TestCase):
    def test_user_creation_form_1(self):
        data = { 'username': 'user1', 'email': 'user1@gmail.com', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        #self.assertEqual(form.is_valid(), True)
        form.is_valid()
        print form.errors

    def test_user_creation_form_2(self):
        # missing password2 field
        data = { 'username': 'user1', 'email': 'user1@gmail.com', 'password1': '12345', 'password': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)

    def test_user_creation_form_3(self):
        # invalid email field
        data = { 'username': 'user1', 'email': 'user1', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)

        data = { 'username': 'user1', 'email': 'user1@123', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)
        
    def test_user_creation_form_4(self):
        # missing password field
        data = { 'username': 'user1', 'email': 'user1@gmail.com', 'password': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)

    def test_user_creation_form_5(self):
        # missing password field
        data = { 'email': 'user1', 'password': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)     

    def test_user_creation_form_6(self):
        data = { 'username': 'userusernamef3r4!@$!@4', 'email': 'user1@gmail.com', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)         
    
    def test_user_creation_form_7(self):
        data = { 'username': 'us', 'email': 'user1@gmail.com', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)    

    def test_user_creation_form_8(self):
        data = { 'username': '1234567891234567891234567890000', 'email': 'user1@gmail.com', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)            

    def test_user_creation_form_9(self):
        # duplicate email field
        data = { 'username': 'user1', 'email': 'user1@gmail.com', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), True)
        form.save()

        data = { 'username': 'user2', 'email': 'user1@gmail.com', 'password1': '12345', 'password2': '12345'}
        form = SnapswerUserCreationForm(data)
        self.assertEqual(form.is_valid(), False)

    def test_user(self):
        user = User(username="gum", email="gum@gmail.com", password="gum1234")
        user.save()

        self.assertEqual(User.objects.count(), 1)

class RegisterAPIViewTest(TestCase):
    def test_login(self):
        client = Client()
        resp = client.post('/accounts_api/register/', 
                          data={"username": "gum2", "email": "gum2@gmail.com", "password":"gum1234"}, 
                          follow=True)
        
        # TODO Find a better way to validate
        string = resp.content
        string = string[1:-1]
        string = re.sub('[\\\]', '', string)
        ret = simplejson.loads(string)
        self.assertEqual(ret[0]["fields"]["user"][0], "gum2")

class AuthenticationFormTest(TestCase):
    def test_auth_1(self):
        user = User(username="gum", email="gum@gmail.com", password="gum1234")
        user.save()

        self.assertEqual(User.objects.count(), 1)

        data = { 'username':'gum', 'password': 'gum1234' }
        form = AuthenticationForm(data=data)
        self.assertEqual(form.is_bound, True)

class ProfileTest(TestCase):
    path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(path, "testoutput")

    def test_profile_create(self):
        user = User(username="gum", email="gum@gmail.com", password="gum1234")
        user.save()

        self.assertEqual(Profile.objects.count(), 1)    

        user.email = "gum1@gmail.com"
        user.save()

        self.assertEqual(Profile.objects.count(), 1)    

        user = User(username="gum2", email="gum@gmail.com", password="gum1234")
        user.save()

        self.assertEqual(Profile.objects.count(), 2)

    def test_profile_pic(self):
        user = User(username="gum", email="gum@gmail.com", password="gum1234")
        user.save()

        #self.assertEqual(user.profile.profile_pic, Constant.DEFAULT_PROFILE_PIC)
    
    @override_settings(MEDIA_ROOT=path)
    def test_profile_form(self):
        user = User(username="gum", email="gum@gmail.com", password="gum1234")
        user.save()

        profile = user.profile
        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "testfiles/images.jpeg")      

        upload_file = open(path, 'rb')
        file_dic = {"profile_pic": SimpleUploadedFile(upload_file.name, upload_file.read())}

        data = {"first_name" : "Gum",
                "last_name" : "Lei"}

        form = ProfileForm(data, file_dic, instance=profile)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)
        form.save()

        self.assertEqual(profile.user.first_name, "Gum")
        self.assertEqual(profile.user.last_name, "Lei")
        self.assertEqual(profile.user.email, "")
        
        # see if first name and last name get updated to empty
        data = {"email": "gum1@gmail.com"}
        upload_file = open(path, 'rb')
        file_dic = {"profile_pic": SimpleUploadedFile(upload_file.name, upload_file.read())}

        form = ProfileForm(data, file_dic, instance=profile)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), True)
        form.save()

        self.assertEqual(profile.user.first_name, "")
        self.assertEqual(profile.user.last_name, "")
        self.assertEqual(profile.user.email, "gum1@gmail.com")           

    @override_settings(MEDIA_ROOT=path)
    def test_profile_form_validation(self):
        user = User(username="gum", email="gum@gmail.com", password="gum1234")
        user.save()

        profile = user.profile
        path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(path, "testfiles/images.jpeg")      

        upload_file = open(path, 'rb')
        file_dic = {"profile_pic": SimpleUploadedFile(upload_file.name, upload_file.read())}

        data = {"first_name" : "Gum123",
                "last_name" : "Lei123131",}

        form = ProfileForm(data, file_dic, instance=profile)
        self.assertEqual(form.is_bound, True)
        self.assertEqual(form.is_valid(), False)

class FollowUserTest(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        self.u1.save()

        self.u2 = User.objects.create_user("tester2", "tester2@snapswer.com", "12345")
        self.u2.save()

        self.u3 = User.objects.create_user("tester3", "tester2@snapswer.com", "12345")
        self.u3.save()

    def test_follow_user(self):
        client = Client()
        ret = client.login(username="tester", password="12345")

        resp = client.post('/accounts/follow/user/' + str(self.u2.id) + "/", follow=True)

        self.assertEqual(self.u2.profile.get_follow_count(), 1)

        resp = client.post('/accounts/follow/user/' + str(self.u2.id) + "/", follow=True)

        self.assertEqual(self.u2.profile.get_follow_count(), 0)

        resp = client.post('/accounts/follow/user/' + str(self.u2.id) + "/", follow=True)

        self.assertEqual(self.u2.profile.get_follow_count(), 1)

        # test invalid
        resp = client.post('/accounts/follow/user/999/', follow=True)

        self.assertEqual(self.u2.profile.get_follow_count(), 1)

        # test follow itself
        resp = client.post('/accounts/follow/user/' + str(self.u1.id) + "/", follow=True)

        self.assertEqual(self.u1.profile.get_follow_count(), 0)
        self.assertEqual(self.u2.profile.get_follow_count(), 1)        

        
class TestUserAnswerView(TestCase):
    def setUp(self):
        self.u1 = User.objects.create_user("tester", "tester@snapswer.com", "12345")
        self.u1.save()

        category = Category()
        category.name = "temp"
        category.save()

    def tearDown(self):
        cache.clear()

    def test_response(self):
        question = Question()
        question.title = "This is question"
        question.owner = self.u1   
        question.category = Category.objects.get(id=1)
        question.save()     

        for i in range(3):
            data = {"detail" : "It is chicken"}

            path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(path, "testfiles/images.jpeg")
            
            upload_file = open(path, 'rb')
            file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
            
            form = AnswerForm(data, file_dic)
            form.save(user=self.u1, question=question)

        client = Client()
        resp = client.get('/accounts/tester/answers/?index=0')

        data = json.loads(resp.content)
        self.assertEqual(len(data["data"]["answers"]), 3)

class TestPublicProfileBoardView(SnapswerTestCase):
    def tearDown(self):
        cache.clear()

    def setUp(self):
        self.view_user = User.objects.get(id=1)
        self.view_user.username = "test1"
        self.view_user.save()

        self.view_user_2 = User.objects.get(id=2)
        self.view_user_2.username = "test2"
        self.view_user_2.save()

        self.view_user_3 = User.objects.get(id=3)
        self.view_user_3.username = "test3"
        self.view_user_3.save()

        for i in range(3):
            question = Question()
            question.title = "This is question"
            question.owner = self.view_user
            question.category = Category.objects.get(id=1)
            question.save()          

        for i in range(3):
            question = Question.objects.get(id=i+1)
            # user 2
            data = {"detail" : "It is chicken"}

            path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(path, "testfiles/images.jpeg")
            
            upload_file = open(path, 'rb')
            file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
            
            form = AnswerForm(data, file_dic)
            form.save(user=self.view_user_2, question=question)

            # user 3
            data = {"detail" : "It is chicken"}

            path = os.path.abspath(os.path.dirname(__file__))
            path = os.path.join(path, "testfiles/images.jpeg")
            
            upload_file = open(path, 'rb')
            file_dic = {"image": SimpleUploadedFile(upload_file.name, upload_file.read())}
            
            form = AnswerForm(data, file_dic)
            form.save(user=self.view_user_3, question=question)


    def test_snapswer(self):
        data = { "index" : 0 }
        client = Client()
        resp = client.get("/accounts/board/test2/snapswer/", data, follow=True)
        
        data = json.loads(resp.content)
        self.assertEqual(len(data["data"]["answers"]), 3)

    def test_question(self):
        data = { "index" : 0 }
        client = Client()
        resp = client.get("/accounts/board/test1/question/", data, follow=True)

        data = json.loads(resp.content)
        self.assertEqual(len(data["data"]["summaries"]), 3)

    def test_following(self):
        data = { "index" : 0 }
        client = Client()
        resp = client.get("/accounts/board/test2/following/", data, follow=True)

        data = json.loads(resp.content)
        self.assertEqual(len(data["data"]), 0)

        self.view_user.profile.followers.add(User.objects.get(id=2))

        data = { "index" : 0 }
        client = Client()
        resp = client.get("/accounts/board/test2/following/", data, follow=True)

        data = json.loads(resp.content)

        self.assertEqual(len(data["data"]["summaries"]), 3)
