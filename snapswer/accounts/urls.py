from django.conf.urls import patterns, url
import views
import core.views
import content.views

urlpatterns = patterns('',
	url(r'^login/$', views.LoginView.as_view(), name="login"),
	url(r'^register/$', views.RegisterView.as_view(), name="register"),
	url(r'^logout/$', views.LoginOutView.as_view(), name="logout"),
	url(r'^profile/$', views.ProfileView.as_view(), name="profile"),
	url(r'^password/$', views.PasswordChangeView.as_view(), name="password"),
	url(r'^follow/user/(\d+)/$', core.views.FollowView.as_view(follow_type="user"), name="follow_user"),
	url(r'^(?P<username>\w+)/answers/$', views.UserAnswerView.as_view(), name="user_answers"),
	url(r'^board/(?P<username>\w+)/(?P<board_type>\w+)/$', views.PublicProfileBoard.as_view(), name="public_snapswer_board"),
)