class Constant:
	DEFAULT_PROFILE_PIC = 'users/default_profile_image.jpg'
	BOARD_TYPE_SNAPSWER = 'snapswer'
	BOARD_TYPE_QUESTION = 'question'
	BOARD_TYPE_FOLLOWING = 'following'
	BOARD_TYPE_VOTED = 'voted'
	