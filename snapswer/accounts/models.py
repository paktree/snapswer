from django.db import models
from core.models import TimeStampedModel
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from .const import Constant
import os
import time
import random
import string
from django_facebook.models import FacebookProfileModel
from django_facebook import signals
from django_facebook.utils import get_user_model
from core.models import FollowableModel

def profile_pic_name_generator(instance, filename):
    name, ext = os.path.splitext(filename)
    new_name = str(time.time()).join(random.choice(string.ascii_uppercase + string.digits) for x in range(3))
    new_name = new_name.translate(None, ".")
    new_name = new_name + ext
    new_name = os.path.join('users/', new_name)
    return new_name	

class Profile(TimeStampedModel, FacebookProfileModel, FollowableModel):
	user = models.OneToOneField('auth.User')
	profile_pic = models.ImageField('Profile Picture', 
									upload_to=profile_pic_name_generator,
									default=Constant.DEFAULT_PROFILE_PIC)
	
	about = models.TextField(max_length=500)

	@receiver(post_save, sender=User)
	def user_save_handler(sender, instance, created, **kwargs):
		if created == True:
			profile = Profile()
			profile.user = instance
			profile.save()

# CPL - Handle facebook post creation handling of migration some of facebook profile data to original profile data
def post_facebook_update(sender, user, profile, facebook_data, **kwargs):
    profile.profile_pic = profile.image
    profile.save()
 
signals.facebook_post_update.connect(post_facebook_update, sender=get_user_model())
