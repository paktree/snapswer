from django.forms import ModelForm
from django import forms
from .models import Profile
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import authenticate

class SnapswerUserCreationForm(UserCreationForm):
    username = forms.RegexField(min_length=4, 
                                max_length=30, 
                                regex=r'^[\w.]+$',
                                error_messages={'invalid': _("Username can use letters, numbers and periods.")})

    email = forms.EmailField()

    def clean_email(self):
        # try to find duplicate email
        email = self.cleaned_data["email"]
        qs = User.objects.filter(email=email)

        if len(qs) != 0:
            raise forms.ValidationError("An other account has been registered with this email.")

        return email          

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class ProfileForm(ModelForm):

    first_name = forms.RegexField(required=False, 
                                  max_length=30, 
                                  regex=r'^[a-zA-Z]+$',
                                  error_messages={'invalid': _("First name can use letters only.")})

    last_name = forms.RegexField(required=False,
                                max_length=30, 
                                regex=r'^[a-zA-Z]+$',
                                error_messages={'invalid': _("First name can use letters only.")})

    email = forms.EmailField(required=True)


    class Meta:
        model = Profile
        fields = ["profile_pic", ]

        widgets = {
            "profile_pic": forms.FileInput(),
        }

    def save(self, commit=True):
        profile = super(ProfileForm, self).save(commit=False)
        profile.profile_pic = self.cleaned_data["profile_pic"]
        
        user = profile.user
        user.last_name = self.cleaned_data["last_name"]
        user.first_name = self.cleaned_data["first_name"]
        user.email = self.cleaned_data["email"]

        if commit:
            profile.save()
            user.save()

        return profile
        
class SnapswerAuthenticaionForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(label=_("Username"), min_length=5, max_length=30)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': _("Please enter a correct username and password. "
                           "Note that both fields are case-sensitive."),
        'no_cookies': _("Your Web browser doesn't appear to have cookies "
                        "enabled. Cookies are required for logging in."),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        If request is passed in, the form will validate that cookies are
        enabled. Note that the request (a HttpRequest object) must have set a
        cookie with the key TEST_COOKIE_NAME and value TEST_COOKIE_VALUE before
        running this validation.
        """
        self.request = request
        self.user_cache = None
        super(SnapswerAuthenticaionForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            if '@' in username:
                user_obj = User.objects.filter(email=username)[:1]
                if not user_obj:
                    raise forms.ValidationError(self.error_messages['invalid_login'])
                else:
                    username = user_obj[0].username 
            
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'])
            elif not self.user_cache.is_active:
                raise forms.ValidationError(self.error_messages['inactive'])
        self.check_for_test_cookie()
        return self.cleaned_data

    def check_for_test_cookie(self):
        if self.request and not self.request.session.test_cookie_worked():
            raise forms.ValidationError(self.error_messages['no_cookies'])

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache    